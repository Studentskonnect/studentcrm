<?php
$languageStrings = array(
'Canada'	=>	'Canada',
'China'	=>	'China',
'France'	=>	'France',
'Germany'	=>	'Germany',
'Ireland'	=>	'Ireland',
'Italy'	=>	'Italy',
'Malaysia'	=>	'Malaysia',
'New Zealand'	=>	'New Zealand',
'Russia'	=>	'Russia',
'Scotland'	=>	'Scotland',
'Singapore'	=>	'Singapore',
'Spain'	=>	'Spain',
'Switzerland'	=>	'Switzerland',
'United Kingdom'	=>	'United Kingdom',
'United Sates of America'	=>	'United Sates of America',
'Other'	=>	'Other',
);