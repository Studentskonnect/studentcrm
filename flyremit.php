<?php

//Users/salim/NetBeansProjects/vt71/AirlineTicket.php

$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = Vtiger_Module::getInstance('Contacts');
$infoBlock = Vtiger_Block::getInstance('Flyremit Details', $module);
if (!$infoBlock) {
    $infoBlock = new Vtiger_Block();
    $infoBlock->label = 'Flyremit Details';
    $module->addBlock($infoBlock);
}

$oshMarginField = Vtiger_Field::getInstance('osh_margin', $module);
if (!$oshMarginField) {
    $oshMarginField = new Vtiger_Field();
    $oshMarginField->name = 'osh_margin';
    $oshMarginField->label = 'OSH Margin';
    $oshMarginField->table = $module->basetable;
    $oshMarginField->column = 'osh_margin';
    $oshMarginField->columntype = 'INT(3)';
    $oshMarginField->uitype = 7;
    $oshMarginField->typeofdata = 'NN~O~3,0';
    $oshMarginField->displaytype = 1;
    $infoBlock->addField($oshMarginField);
}
$agentMarginField = Vtiger_Field::getInstance('agent_margin', $module);
if (!$agentMarginField) {
    $agentMarginField = new Vtiger_Field();
    $agentMarginField->name = 'agent_margin';
    $agentMarginField->label = 'Agent Margin';
    $agentMarginField->table = $module->basetable;
    $agentMarginField->column = 'agent_margin';
    $agentMarginField->columntype = 'INT(3)';
    $agentMarginField->uitype = 7;
    $agentMarginField->displaytype = 1;
    $agentMarginField->typeofdata = 'NN~O~3,0';
    $infoBlock->addField($agentMarginField);
}

$markupOptionField = Vtiger_Field::getInstance('markup_option', $module);
if (!$markupOptionField) {
    $markupOptionField = new Vtiger_Field();
    $markupOptionField->name = 'markup_option';
    $markupOptionField->label = 'Markup Option';
    $markupOptionField->table = $module->basetable;
    $markupOptionField->column = 'markup_option';
    $markupOptionField->columntype = 'INT(1)';
    $markupOptionField->uitype = 56;
    $markupOptionField->typeofdata = 'C~O';
    $infoBlock->addField($markupOptionField);
}

$registratioStatusField = Vtiger_Field::getInstance('fly_status', $module);
if (!$registratioStatusField) {
    $registratioStatusField = new Vtiger_Field();
    $registratioStatusField->name = 'fly_status';
    $registratioStatusField->label = 'Registration Status';
    $registratioStatusField->table = $module->basetable;
    $registratioStatusField->column = 'fly_status';
    $registratioStatusField->columntype = 'VARCHAR(64)';
    $registratioStatusField->uitype = 56;
    $registratioStatusField->typeofdata = 'V~O';
    $registratioStatusField->displaytype = 2;
    $infoBlock->addField($registratioStatusField);
}
