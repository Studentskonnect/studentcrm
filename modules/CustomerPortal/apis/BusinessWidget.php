<?php
///Users/salim/NetBeansProjects/vt71/modules/CustomerPortal/apis/BusinessWidget.php
class CustomerPortal_BusinessWidget extends CustomerPortal_API_Abstract {

    protected function processRetrieve(CustomerPortal_API_Request $request) {
        $db = PearDatabase::getInstance();

        $sql = "SELECT 
                    (SELECT 'Flight Ticket') as category, SUM(C.cf_993) as total
                from
                    vtiger_airlineticket AS A
                        join
                    vtiger_crmentity AS B ON (B.crmid = A.airlineticketid)
                        JOIN
                    vtiger_airlineticketcf AS C ON (A.airlineticketid = C.airlineticketid)
                WHERE
                    A.contact_id = ? and B.deleted = 0
                        AND B.createdtime <= NOW()
                        AND B.createdtime >= date_add(NOW(), interval - 12 month) 
                UNION SELECT 
                    (SELECT 'Student Protection') as category,SUM(C.cf_1002) as total
                from
                    vtiger_insurancecertificate AS A
                        join
                    vtiger_crmentity AS B ON (B.crmid = A.insurancecertificateid)
                        JOIN
                    vtiger_insurancecertificatecf AS C ON (A.insurancecertificateid = C.insurancecertificateid)
                WHERE
                    A.contact_id = ? and B.deleted = 0
                        AND B.createdtime <= NOW()
                        AND B.createdtime >= date_add(NOW(), interval - 12 month)
                UNION SELECT 
                    (SELECT 'Sim Card') as category, SUM(A.amount_paid) as total
                from
                    vtiger_simconnection AS A
                        join
                    vtiger_crmentity AS B ON (B.crmid = A.simconnectionid)
                WHERE
                    A.contact_id = ? and B.deleted = 0
                        AND B.createdtime <= NOW()
                        AND B.createdtime >= date_add(NOW(), interval - 12 month)
                UNION SELECT 
                    (SELECT 'Accommodation') as category, count(C.cf_1083) as total
                from
                    vtiger_accommodation AS A
                        JOIN
                    vtiger_crmentity AS B ON (B.crmid = A.Accommodationid)
                        JOIN
                   vtiger_accommodationcf  AS C ON (A.Accommodationid = C.Accommodationid)
                WHERE
                    C.cf_1105 = ? and B.deleted = 0
                        AND B.createdtime <= NOW()
                        AND B.createdtime >= date_add(NOW(), interval - 12 month) ";


        $sqlResult = $db->pquery($sql, array($this->getActiveCustomer()->id,$this->getActiveCustomer()->id,$this->getActiveCustomer()->id,$this->getActiveCustomer()->id));
        $data = Array();
        $color = Array('#e56985','#5ca4f0','#7068b5','#000000');

        while ($row = $db->fetch_array($sqlResult)) {
            
            $data[] = Array(
                'name' => $row['category'],
                'y' => ($row['total'] > 0) ? $row['total']: 0,
                'color' => array_pop($color)
            
            );
            
        }

        return $data;
    }

    function process(CustomerPortal_API_Request $request) {
        $response = new CustomerPortal_API_Response();
        $current_user = $this->getActiveUser();

        if ($current_user) {
            $record = $this->processRetrieve($request);
            $response->setResult($record);
        }
        return $response;
    }

}
