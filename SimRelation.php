<?php
$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = Vtiger_Module::getInstance('SimConnection');

$relatedToField = Vtiger_Field::getInstance('contact_id', $module);
$relatedStudentField = Vtiger_Field::getInstance('studentid', $module);

$contactsModule = Vtiger_Module::getInstance('Contacts');
$contactsModule->setRelatedList(
        $module, 'Sim Connection', Array(), 'get_dependents_list', $relatedToField->id
);

$studentModule = Vtiger_Module::getInstance('Student');
$studentModule->setRelatedList(
        $module, 'Sim Connection', Array(), 'get_dependents_list', $relatedStudentField->id
);
