<?php
//Users/salim/NetBeansProjects/vt71/modules/CustomerPortal/apis/FetchCountryList.php
class CustomerPortal_FetchCountryList extends CustomerPortal_API_Abstract {

    protected function processRetrieve(CustomerPortal_API_Request $request) {
        $db = PearDatabase::getInstance();
        $sql = "select country_list as country_name,country_code from vtiger_country_list";


        $sqlResult = $db->pquery($sql, array());
        $result = Array();

        while ($row = $db->fetch_array($sqlResult)) {
           $result[] = array('id'=>$row['country_code'],'name'=>$row['country_name']); 
        }

        return $result;
    }

    function process(CustomerPortal_API_Request $request) {
        $response = new CustomerPortal_API_Response();
        $current_user = $this->getActiveUser();

        if ($current_user) {
            $record = $this->processRetrieve($request);
            $response->setResult($record);
        }
        return $response;
    }

}
