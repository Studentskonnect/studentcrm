<?php

//Users/salim/NetBeansPlans/vt71/InsurancePlan.php

$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = Vtiger_Module::getInstance('InsurancePlan');
if (!$module) {
    $module = new Vtiger_Module();
    $module->name = 'InsurancePlan';
    $module->parent = 'Sales';
    $module->save();
    $module->initTables();
    $module->initWebservice();
}

//     info block
$infoBlock = Vtiger_Block::getInstance('LBL_INSURANCEPLAN_DETAILS', $module);
if (!$infoBlock) {
    $infoBlock = new Vtiger_Block();
    $infoBlock->label = 'LBL_INSURANCEPLAN_DETAILS';
    $module->addBlock($infoBlock);
}

//Description Details
$descriptionBlock = Vtiger_Block::getInstance('LBL_DESCRIPTION_INFORMATION', $module);
if (!$descriptionBlock) {
    $descriptionBlock = new Vtiger_Block();
    $descriptionBlock->label = 'LBL_DESCRIPTION_INFORMATION';
    $module->addBlock($descriptionBlock);
}

$pnrnoField = Vtiger_Field::getInstance('planname', $module);
if (!$pnrnoField) {
    $pnrnoField = new Vtiger_Field();
    $pnrnoField->name = 'planname';
    $pnrnoField->label = 'Plan Name';
    $pnrnoField->table = $module->basetable;
    $pnrnoField->column = 'planname';
    $pnrnoField->columntype = 'VARCHAR(128)';
    $pnrnoField->uitype = 1;
    $pnrnoField->typeofdata = 'V~M';
    $infoBlock->addField($pnrnoField);
    /** Creates the field and adds to block */
    $module->setEntityIdentifier($pnrnoField);
}

$typeOfPlanField = Vtiger_Field::getInstance('type_of_plan', $module);
if (!$typeOfPlanField) {
    $typeOfPlanField = new Vtiger_Field();
    $typeOfPlanField->name = 'type_of_plan';
    $typeOfPlanField->label = 'Type Of Plan';
    $typeOfPlanField->column = 'type_of_plan';
    $typeOfPlanField->columntype = 'VARCHAR(100)';
    $typeOfPlanField->uitype = 16;
    $typeOfPlanField->typeofdata = 'V~O';
    $infoBlock->addField($typeOfPlanField);
    $typeOfPlanField->setPicklistValues(array('Value Pro', 'Smart Pro', 'Style Pro'));
}

$countryField = Vtiger_Field::getInstance('country', $module);
if (!$countryField) {
    $countryField = new Vtiger_Field();
    $countryField->name = 'country';
    $countryField->label = 'Country';
    $countryField->column = 'country';
    $countryField->columntype = 'VARCHAR(100)';
    $countryField->uitype = 16;
    $countryField->typeofdata = 'V~O';
    $infoBlock->addField($countryField);
    $countryField->setPicklistValues(array('Including USA & Canada', 'Excluding USA & Canada'));
}

$daysField = Vtiger_Field::getInstance('no_of_days', $module);
if (!$daysField) {
    $daysField = new Vtiger_Field();
    $daysField->name = 'no_of_days';
    $daysField->label = 'No of Days';
    $daysField->table = $module->basetable;
    $daysField->column = 'rate';
    $daysField->columntype = 'INT(6)';
    $daysField->uitype = 1;
    $daysField->typeofdata = 'V~M';
    $infoBlock->addField($daysField);
}

$rateField = Vtiger_Field::getInstance('rate', $module);
if (!$rateField) {
    $rateField = new Vtiger_Field();
    $rateField->name = 'rate';
    $rateField->label = 'Rate';
    $rateField->table = $module->basetable;
    $rateField->column = 'rate';
    $rateField->columntype = 'INT(12)';
    $rateField->uitype = 71;
    $rateField->typeofdata = 'V~M';
    $infoBlock->addField($rateField);
}

$sumInsuredField = Vtiger_Field::getInstance('sum_insured', $module);
if (!$sumInsuredField) {
    $sumInsuredField = new Vtiger_Field();
    $sumInsuredField->name = 'sum_insured';
    $sumInsuredField->label = 'Sum Insured';
    $sumInsuredField->table = $module->basetable;
    $sumInsuredField->column = 'sum_insured';
    $sumInsuredField->columntype = 'INT(12)';
    $sumInsuredField->uitype = 71;
    $sumInsuredField->typeofdata = 'V~M';
    $infoBlock->addField($sumInsuredField);
}

$planCodeField = Vtiger_Field::getInstance('plancode', $module);
if (!$planCodeField) {
    $planCodeField = new Vtiger_Field();
    $planCodeField->name = 'plancode';
    $planCodeField->label = 'Plan Code';
    $planCodeField->table = $module->basetable;
    $planCodeField->column = 'plancode';
    $planCodeField->columntype = 'VARCHAR(128)';
    $planCodeField->uitype = 1;
    $planCodeField->typeofdata = 'V~M';
    $infoBlock->addField($planCodeField);
}

/*
 * Field For Assigned To
 */
$assignedToField = Vtiger_Field::getInstance('assigned_user_id', $module);
if (!$assignedToField) {
    $assignedToField = new Vtiger_Field();
    $assignedToField->name = 'assigned_user_id';
    $assignedToField->label = 'Assigned To';
    $assignedToField->table = 'vtiger_crmentity';
    $assignedToField->column = 'smownerid';
    $assignedToField->uitype = 53;
    $assignedToField->typeofdata = 'V~M';
    $infoBlock->addField($assignedToField);
}

$createdTimeField = Vtiger_Field::getInstance('createdtime', $module);
if (!$createdTimeField) {
    $createdTimeField = new Vtiger_Field();
    $createdTimeField->name = 'createdtime';
    $createdTimeField->label = 'Created Time';
    $createdTimeField->table = 'vtiger_crmentity';
    $createdTimeField->column = 'createdtime';
    $createdTimeField->uitype = 70;
    $createdTimeField->typeofdata = 'T~O';
    $createdTimeField->displaytype = 2;
    $infoBlock->addField($createdTimeField);
}
/*
 * Modified Time
 */
$modifiedTimeField = Vtiger_Field::getInstance('modifiedtime', $module);
if (!$modifiedTimeField) {
    $modifiedTimeField = new Vtiger_Field();
    $modifiedTimeField->name = 'modifiedtime';
    $modifiedTimeField->label = 'Modified Time';
    $modifiedTimeField->table = 'vtiger_crmentity';
    $modifiedTimeField->column = 'modifiedtime';
    $modifiedTimeField->uitype = 70;
    $modifiedTimeField->typeofdata = 'T~O';
    $modifiedTimeField->displaytype = 2;
    $infoBlock->addField($modifiedTimeField);
}

$allFilter = Vtiger_Filter::getInstance('All', $module);
if (!$allFilter) {
    $allFilter = new Vtiger_Filter();
    $allFilter->name = 'All';
    $allFilter->isdefault = true;
    $module->addFilter($allFilter);
// Add fields to the filter created
    $allFilter->addField($pnrnoField)
            ->addField($typeOfPlanField, 1)
            ->addField($daysField, 2)
            ->addField($rateField, 3)
            ->addField($sumInsuredField, 4);
}


/** Set sharing access of this module */
$module->setDefaultSharing('Private');
/** Enable and Disable available tools */
$module->enableTools(Array('Import'));
$module->disableTools('Merge');
global $adb;

$adb->pquery('INSERT INTO vtiger_app2tab(tabid,appname,sequence,visible) values(?,?,?,?)', array(getTabid('InsurancePlan'), 'SALES', 30, 1));
$adb->pquery('CREATE TABLE `vtiger_insuranceplan_user_field` (
  `recordid` int(25) NOT NULL,
  `userid` int(25) NOT NULL,
  `starred` varchar(100) DEFAULT NULL,
  KEY `fk_insuranceplanid_vtiger_insuranceplan_user_field` (`recordid`),
  CONSTRAINT `fk_insuranceplanid_vtiger_insuranceplan_user_field` FOREIGN KEY (`recordid`) REFERENCES `vtiger_insuranceplan` (`insuranceplanid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8', Array());
