<?php

//Users/salim/NetBeansPlans/vt71/SimConnection.php

$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = Vtiger_Module::getInstance('SimConnection');
if (!$module) {
    $module = new Vtiger_Module();
    $module->name = 'SimConnection';
    $module->parent = 'Sales';
    $module->save();
    $module->initTables();
    $module->initWebservice();
}

//     info block
$infoBlock = Vtiger_Block::getInstance('LBL_CONNECTION_DETAILS', $module);
if (!$infoBlock) {
    $infoBlock = new Vtiger_Block();
    $infoBlock->label = 'LBL_CONNECTION_DETAILS';
    $module->addBlock($infoBlock);
}

//Description Details
$descriptionBlock = Vtiger_Block::getInstance('LBL_DESCRIPTION_INFORMATION', $module);
if (!$descriptionBlock) {
    $descriptionBlock = new Vtiger_Block();
    $descriptionBlock->label = 'LBL_DESCRIPTION_INFORMATION';
    $module->addBlock($descriptionBlock);
}

$pnrnoField = Vtiger_Field::getInstance('label', $module);
if (!$pnrnoField) {
    $pnrnoField = new Vtiger_Field();
    $pnrnoField->name = 'label';
    $pnrnoField->label = 'Subject';
    $pnrnoField->table = $module->basetable;
    $pnrnoField->column = 'label';
    $pnrnoField->columntype = 'VARCHAR(128)';
    $pnrnoField->uitype = 1;
    $pnrnoField->typeofdata = 'V~M';
    $infoBlock->addField($pnrnoField);
    /** Creates the field and adds to block */
    $module->setEntityIdentifier($pnrnoField);
}


/*
 * Party
 */
$relatedToField = Vtiger_Field::getInstance('contact_id', $module);
if (!$relatedToField) {
    $relatedToField = new Vtiger_Field();
    $relatedToField->name = 'contact_id';
    $relatedToField->label = 'Agent';
    $relatedToField->table = $module->basetable;
    $relatedToField->columntype = 'INT(11)';
    $relatedToField->uitype = 10;
    $relatedToField->typeofdata = 'V~M';
    $infoBlock->addField($relatedToField);
    $relatedToField->setRelatedModules(array('Contacts'));
}
/*
 * Party
 */
$relatedStudentField = Vtiger_Field::getInstance('studentid', $module);
if (!$relatedStudentField) {
    $relatedStudentField = new Vtiger_Field();
    $relatedStudentField->name = 'studentid';
    $relatedStudentField->label = 'Student';
    $relatedStudentField->table = $module->basetable;
    $relatedStudentField->columntype = 'INT(11)';
    $relatedStudentField->uitype = 10;
    $relatedStudentField->typeofdata = 'V~M';
    $infoBlock->addField($relatedStudentField);
    $relatedStudentField->setRelatedModules(array('Student'));
}


/*
 * Date Of supplay
 */
$dobDateField = Vtiger_Field::getInstance('dob_date', $module);
if (!$dobDateField) {
    $dobDateField = new Vtiger_Field();
    $dobDateField->name = 'dob_date';
    $dobDateField->label = 'Date Of birth';
    $dobDateField->table = $module->basetable;
    $dobDateField->column = 'dob_date';
    $dobDateField->columntype = 'DATE';
    $dobDateField->uitype = 5;
    $dobDateField->typeofdata = 'D~O';
    $infoBlock->addField($dobDateField);
}


$ResidenceAddressField = Vtiger_Field::getInstance('residence_address', $module);
if (!$ResidenceAddressField) {
    $ResidenceAddressField = new Vtiger_Field();
    $ResidenceAddressField->name = 'residence_address';
    $ResidenceAddressField->label = 'Residence Address';
    $ResidenceAddressField->table = $module->basetable;
    $ResidenceAddressField->column = 'residence_address';
    $ResidenceAddressField->columntype = 'VARCHAR(500)';
    $ResidenceAddressField->uitype = 21;
    $ResidenceAddressField->typeofdata = 'V~O';
    $infoBlock->addField($ResidenceAddressField);
}
/*
 * Field for PinCode 
 */
$pinCodeField = Vtiger_Field::getInstance('pincode', $module);
if (!$pinCodeField) {
    $pinCodeField = new Vtiger_Field();
    $pinCodeField->name = 'pincode';
    $pinCodeField->label = 'Postal code';
    $pinCodeField->table = $module->basetable;
    $pinCodeField->uitype = 11;
    $pinCodeField->typeofdata = 'V~O';
    $infoBlock->addField($pinCodeField);
}

$DeliveryAddressField = Vtiger_Field::getInstance('delivery_address', $module);
if (!$DeliveryAddressField) {
    $DeliveryAddressField = new Vtiger_Field();
    $DeliveryAddressField->name = 'delivery_address';
    $DeliveryAddressField->label = 'Delivery Address';
    $DeliveryAddressField->table = $module->basetable;
    $DeliveryAddressField->column = 'delivery_address';
    $DeliveryAddressField->columntype = 'VARCHAR(500)';
    $DeliveryAddressField->uitype = 21;
    $DeliveryAddressField->typeofdata = 'V~O';
    $infoBlock->addField($DeliveryAddressField);
}

$AirportPickUpField = Vtiger_Field::getInstance('airport_pickUp', $module);
if (!$AirportPickUpField) {
    $AirportPickUpField = new Vtiger_Field();
    $AirportPickUpField->name = 'airport_pickUp';
    $AirportPickUpField->label = 'Airport PickUp';
    $AirportPickUpField->table = $module->basetable;
    $AirportPickUpField->column = 'airport_pickUp';
    $AirportPickUpField->columntype = 'VARCHAR(128)';
    $AirportPickUpField->uitype = 1;
    $AirportPickUpField->typeofdata = 'V~O';
    $infoBlock->addField($AirportPickUpField);
}

/*
 * Field for Contact number 
 */
$phoneField = Vtiger_Field::getInstance('phone', $module);
if (!$phoneField) {
    $phoneField = new Vtiger_Field();
    $phoneField->name = 'phone';
    $phoneField->label = 'Phone';
    $phoneField->table = $module->basetable;
    $phoneField->uitype = 11;
    $phoneField->typeofdata = 'V~O';
    $infoBlock->addField($phoneField);
}

/*
 * Field for email Address
 */
$emailField = Vtiger_Field::getInstance('email', $module);
if (!$emailField) {
    $emailField = new Vtiger_Field();
    $emailField->name = 'email';
    $emailField->label = 'Email';
    $emailField->table = $module->basetable;
    $emailField->uitype = 13;
    $emailField->typeofdata = 'E~O';
    $infoBlock->addField($emailField);
}


$PassportNumberField = Vtiger_Field::getInstance('passport_number', $module);
if (!$PassportNumberField) {
    $PassportNumberField = new Vtiger_Field();
    $PassportNumberField->name = 'passport_number';
    $PassportNumberField->label = 'Passport Number';
    $PassportNumberField->table = $module->basetable;
    $PassportNumberField->column = 'passport_number';
    $PassportNumberField->columntype = 'VARCHAR(32)';
    $PassportNumberField->uitype = 1;
    $PassportNumberField->typeofdata = 'V~O';
    $infoBlock->addField($AirportPickUpField);
}

$TripDurationField = Vtiger_Field::getInstance('trip_duration', $module);
if (!$TripDurationField) {
    $TripDurationField = new Vtiger_Field();
    $TripDurationField->name = 'trip_duration';
    $TripDurationField->label = 'Trip Duration';
    $TripDurationField->table = $module->basetable;
    $TripDurationField->column = 'trip_duration';
    $TripDurationField->columntype = 'VARCHAR(128)';
    $TripDurationField->uitype = 1;
    $TripDurationField->typeofdata = 'V~O';
    $infoBlock->addField($TripDurationField);
}

$countryField = Vtiger_Field::getInstance('country', $module);
if (!$countryField) {
    $countryField = new Vtiger_Field();
    $countryField->name = 'country';
    $countryField->label = 'Destination Country';
    $countryField->column = 'country';
    $countryField->columntype = 'VARCHAR(100)';
    $countryField->uitype = 1;
    $countryField->typeofdata = 'V~O';
    $infoBlock->addField($countryField);
}

$descriptionField = Vtiger_Field::getInstance('description', $module);
if (!$descriptionField) {
    $descriptionField = new Vtiger_Field();
    $descriptionField->name = 'description';
    $descriptionField->label = 'Plan Detail';
    $descriptionField->table = 'vtiger_crmentity';
    $descriptionField->column = 'description';
    $descriptionField->uitype = 19;
    $descriptionField->typeofdata = 'V~O';
    $descriptionBlock->addField($descriptionField);
    /** Creates the field and adds to block */
}

$rateField = Vtiger_Field::getInstance('amount_paid', $module);
if (!$rateField) {
    $rateField = new Vtiger_Field();
    $rateField->name = 'amount_paid';
    $rateField->label = 'Amount Paid';
    $rateField->table = $module->basetable;
    $rateField->column = 'amount_paid';
    $rateField->columntype = 'INT(12)';
    $rateField->uitype = 71;
    $rateField->typeofdata = 'V~M';
    $infoBlock->addField($rateField);
}

/*
 * Field For Assigned To
 */
$assignedToField = Vtiger_Field::getInstance('assigned_user_id', $module);
if (!$assignedToField) {
    $assignedToField = new Vtiger_Field();
    $assignedToField->name = 'assigned_user_id';
    $assignedToField->label = 'Assigned To';
    $assignedToField->table = 'vtiger_crmentity';
    $assignedToField->column = 'smownerid';
    $assignedToField->uitype = 53;
    $assignedToField->typeofdata = 'V~M';
    $infoBlock->addField($assignedToField);
}

$createdTimeField = Vtiger_Field::getInstance('createdtime', $module);
if (!$createdTimeField) {
    $createdTimeField = new Vtiger_Field();
    $createdTimeField->name = 'createdtime';
    $createdTimeField->label = 'Created Time';
    $createdTimeField->table = 'vtiger_crmentity';
    $createdTimeField->column = 'createdtime';
    $createdTimeField->uitype = 70;
    $createdTimeField->typeofdata = 'T~O';
    $createdTimeField->displaytype = 2;
    $infoBlock->addField($createdTimeField);
}
/*
 * Modified Time
 */
$modifiedTimeField = Vtiger_Field::getInstance('modifiedtime', $module);
if (!$modifiedTimeField) {
    $modifiedTimeField = new Vtiger_Field();
    $modifiedTimeField->name = 'modifiedtime';
    $modifiedTimeField->label = 'Modified Time';
    $modifiedTimeField->table = 'vtiger_crmentity';
    $modifiedTimeField->column = 'modifiedtime';
    $modifiedTimeField->uitype = 70;
    $modifiedTimeField->typeofdata = 'T~O';
    $modifiedTimeField->displaytype = 2;
    $infoBlock->addField($modifiedTimeField);
}

$allFilter = Vtiger_Filter::getInstance('All', $module);
if (!$allFilter) {
    $allFilter = new Vtiger_Filter();
    $allFilter->name = 'All';
    $allFilter->isdefault = true;
    $module->addFilter($allFilter);
// Add fields to the filter created
    $allFilter->addField($pnrnoField)
            ->addField($relatedToField, 1)
            ->addField($relatedStudentField, 2)
            ->addField($phoneField, 3)
            ->addField($emailField, 4)
            ->addField($dobDateField, 5)
            ->addField($countryField, 6);
}


/** Set sharing access of this module */
$module->setDefaultSharing('Private');
/** Enable and Disable available tools */
$module->enableTools(Array('Import'));
$module->disableTools('Merge');
global $adb;

$adb->pquery('INSERT INTO vtiger_app2tab(tabid,appname,sequence,visible) values(?,?,?,?)', array(getTabid('SimConnection'), 'SALES', 30, 1));
$adb->pquery('CREATE TABLE `vtiger_simconnection_user_field` (
  `recordid` int(25) NOT NULL,
  `userid` int(25) NOT NULL,
  `starred` varchar(100) DEFAULT NULL,
  KEY `fk_simconnectionid_vtiger_simconnection_user_field` (`recordid`),
  CONSTRAINT `fk_simconnectionid_vtiger_simconnection_user_field` FOREIGN KEY (`recordid`) REFERENCES `vtiger_simconnection` (`simconnectionid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8', Array());


//Portal Info
$adb->pquery("insert into vtiger_customerportal_fields values (?,?,1)", array(getTabid('SimConnection'), '{"label":1,"contact_id":0,"studentid":0,"assigned_user_id":0}'));
$adb->pquery('insert into vtiger_customerportal_tabs values (?,1,15,1,1)', array(getTabid('SimConnection')));
$adb->pquery("insert into vtiger_customerportal_prefs values (?,'showrelatedinfo',1)", array(getTabid('SimConnection')));
