<?php
///Users/salim/NetBeansProjects/vt71/StudentChanges.php

$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = Vtiger_Module::getInstance('Student');
$infoBlock = Vtiger_Block::getInstance('LBL_STUDENT_DETAILS', $module);


$countryField = Vtiger_Field::getInstance('state', $module);
$countryField->setPicklistValues(array('Maharashtra','Tamil Nadu','Gujarat','Karnataka','Uttar Pradesh','West Bengal','Rajasthan','Telangana','Andhra Pradesh','Kerala','Madhya Pradesh','Haryana','Delhi','Bihar','Punjab','Odisha','Assam','Jharkhand','Chhattisgarh','Uttarakhand','Himachal Pradesh','Jammu and Kashmir','Goa','Tripura','Puducherry','Chandigarh','Meghalaya','Arunachal Pradesh','Manipur','Sikkim','Nagaland','Mizoram','Andaman and Nicobar Islands'));

