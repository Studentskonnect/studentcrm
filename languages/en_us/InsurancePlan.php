<?php
/*+**********************************************************************************
 * Created by : Salim
 * Email : salimcmd@gmail.com, salim@oztro.com
 ************************************************************************************/
$languageStrings = array(
// Basic Strings
    'InsurancePlan' => 'Insurance Plans',
    'SINGLE_InsurancePlan' => 'Insurance Plan',
    'LBL_ADD_RECORD' => 'Add Insurance Plan',
    'LBL_RECORDS_LIST' => 'Insurance Plan List',
    // Blocks
    'LBL_INSURANCEPLAN_DETAILS' => 'Insurance Plan Details',
    //Field Labels
    "Phone" => "Phone",
);

$jsLanguageStrings = array();
