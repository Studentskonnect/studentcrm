<?php

//Users/salim/NetBeansProjects/vt71/InsuranceCertificate.php

$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = Vtiger_Module::getInstance('InsuranceCertificate');
if (!$module) {
    $module = new Vtiger_Module();
    $module->name = 'InsuranceCertificate';
    $module->parent = 'Sales';
    $module->save();
    $module->initTables();
    $module->initWebservice();
}
/*
 * 
 * Blocks for Architects Module
 * 
 */
//     info block
$infoBlock = Vtiger_Block::getInstance('LBL_POLICY_DETAILS', $module);
if (!$infoBlock) {
    $infoBlock = new Vtiger_Block();
    $infoBlock->label = 'LBL_POLICY_DETAILS';
    $module->addBlock($infoBlock);
}

//Description Details
$descriptionBlock = Vtiger_Block::getInstance('LBL_DESCRIPTION_INFORMATION', $module);
if (!$descriptionBlock) {
    $descriptionBlock = new Vtiger_Block();
    $descriptionBlock->label = 'LBL_DESCRIPTION_INFORMATION';
    $module->addBlock($descriptionBlock);
}

$InsuranceCertificateField = Vtiger_Field::getInstance('certificate_number', $module);
if (!$InsuranceCertificateField) {
    $InsuranceCertificateField = new Vtiger_Field();
    $InsuranceCertificateField->name = 'certificate_number';
    $InsuranceCertificateField->label = 'Certificate Number';
    $InsuranceCertificateField->table = $module->basetable;
    $InsuranceCertificateField->column = 'certificate_number';
    $InsuranceCertificateField->columntype = 'VARCHAR(128)';
    $InsuranceCertificateField->uitype = 255;
    $InsuranceCertificateField->typeofdata = 'V~M';
    $infoBlock->addField($InsuranceCertificateField);
    /** Creates the field and adds to block */
    $module->setEntityIdentifier($InsuranceCertificateField);
}

$DocumentField = Vtiger_Field::getInstance('document_url', $module);
if (!$DocumentField) {
    $DocumentField = new Vtiger_Field();
    $DocumentField->name = 'document_url';
    $DocumentField->label = 'Document';
    $DocumentField->table = $module->basetable;
    $DocumentField->column = 'document_url';
    $DocumentField->columntype = 'VARCHAR(258)';
    $DocumentField->uitype = 17;
    $DocumentField->typeofdata = 'V~M';
    $infoBlock->addField($DocumentField);
}


$ReferenceField = Vtiger_Field::getInstance('reference_no', $module);
if (!$ReferenceField) {
    $ReferenceField = new Vtiger_Field();
    $ReferenceField->name = 'reference_no';
    $ReferenceField->label = 'Reference';
    $ReferenceField->table = $module->basetable;
    $ReferenceField->column = 'reference_no';
    $ReferenceField->columntype = 'VARCHAR(128)';
    $ReferenceField->uitype = 1;
    $ReferenceField->typeofdata = 'V~O';
    $infoBlock->addField($ReferenceField);
}


$ClaimCodeField = Vtiger_Field::getInstance('claim_code', $module);
if (!$ClaimCodeField) {
    $ClaimCodeField = new Vtiger_Field();
    $ClaimCodeField->name = 'claim_code';
    $ClaimCodeField->label = 'Claim Code';
    $ClaimCodeField->table = $module->basetable;
    $ClaimCodeField->column = 'claim_code';
    $ClaimCodeField->columntype = 'VARCHAR(128)';
    $ClaimCodeField->uitype = 1;
    $ClaimCodeField->typeofdata = 'V~O';
    $infoBlock->addField($ClaimCodeField);
}


/*
 * Party
 */
$relatedToField = Vtiger_Field::getInstance('contact_id', $module);
if (!$relatedToField) {
    $relatedToField = new Vtiger_Field();
    $relatedToField->name = 'contact_id';
    $relatedToField->label = 'Agent';
    $relatedToField->table = $module->basetable;
    $relatedToField->columntype = 'INT(11)';
    $relatedToField->uitype = 10;
    $relatedToField->typeofdata = 'V~M';
    $infoBlock->addField($relatedToField);
    $relatedToField->setRelatedModules(array('Contacts'));
}
/*
 * Party
 */
$relatedStudentField = Vtiger_Field::getInstance('studentid', $module);
if (!$relatedStudentField) {
    $relatedStudentField = new Vtiger_Field();
    $relatedStudentField->name = 'studentid';
    $relatedStudentField->label = 'Student';
    $relatedStudentField->table = $module->basetable;
    $relatedStudentField->columntype = 'INT(11)';
    $relatedStudentField->uitype = 10;
    $relatedStudentField->typeofdata = 'V~M';
    $infoBlock->addField($relatedStudentField);
    $relatedStudentField->setRelatedModules(array('Student'));
}


/*
 * Field For Assigned To
 */
$assignedToField = Vtiger_Field::getInstance('assigned_user_id', $module);
if (!$assignedToField) {
    $assignedToField = new Vtiger_Field();
    $assignedToField->name = 'assigned_user_id';
    $assignedToField->label = 'Assigned To';
    $assignedToField->table = 'vtiger_crmentity';
    $assignedToField->column = 'smownerid';
    $assignedToField->uitype = 53;
    $assignedToField->typeofdata = 'V~M';
    $infoBlock->addField($assignedToField);
}

$createdTimeField = Vtiger_Field::getInstance('createdtime', $module);
if (!$createdTimeField) {
    $createdTimeField = new Vtiger_Field();
    $createdTimeField->name = 'createdtime';
    $createdTimeField->label = 'Created Time';
    $createdTimeField->table = 'vtiger_crmentity';
    $createdTimeField->column = 'createdtime';
    $createdTimeField->uitype = 70;
    $createdTimeField->typeofdata = 'T~O';
    $createdTimeField->displaytype = 2;
    $infoBlock->addField($createdTimeField);
}
/*
 * Modified Time
 */
$modifiedTimeField = Vtiger_Field::getInstance('modifiedtime', $module);
if (!$modifiedTimeField) {
    $modifiedTimeField = new Vtiger_Field();
    $modifiedTimeField->name = 'modifiedtime';
    $modifiedTimeField->label = 'Modified Time';
    $modifiedTimeField->table = 'vtiger_crmentity';
    $modifiedTimeField->column = 'modifiedtime';
    $modifiedTimeField->uitype = 70;
    $modifiedTimeField->typeofdata = 'T~O';
    $modifiedTimeField->displaytype = 2;
    $infoBlock->addField($modifiedTimeField);
}

$allFilter = Vtiger_Filter::getInstance('All', $module);
if (!$allFilter) {
    $allFilter = new Vtiger_Filter();
    $allFilter->name = 'All';
    $allFilter->isdefault = true;
    $module->addFilter($allFilter);
// Add fields to the filter created
    $allFilter->addField($relatedToField)
            ->addField($relatedStudentField, 1)
            ->addField($InsuranceCertificateField, 2)
            ->addField($DocumentField, 3)
            ->addField($ClaimCodeField, 4);
}

/** Set sharing access of this module */
$module->setDefaultSharing('Private');
/** Enable and Disable available tools */
$module->enableTools(Array('Import'));
$module->disableTools('Merge');
global $adb;

$adb->pquery('INSERT INTO vtiger_app2tab(tabid,appname,sequence,visible) values(?,?,?,?)', array(getTabid('InsuranceCertificate'), 'SALES', 30, 1));
$adb->pquery('CREATE TABLE `vtiger_insurancecertificate_user_field` (
  `recordid` int(25) NOT NULL,
  `userid` int(25) NOT NULL,
  `starred` varchar(100) DEFAULT NULL,
  KEY `fk_insurancecertificateid_vtiger_insurancecertificate_user_field` (`recordid`),
  CONSTRAINT `fk_insurancecertificateid_vtiger_insurancecertificate_user_field` FOREIGN KEY (`recordid`) REFERENCES `vtiger_insurancecertificate` (`insurancecertificateid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8', Array());

$contactsModule = Vtiger_Module::getInstance('Contacts');
$contactsModule->setRelatedList(
        $module, 'Insurance Certificates', Array(), 'get_dependents_list', $relatedToField->id
);

$studentModule = Vtiger_Module::getInstance('Student');
$studentModule->setRelatedList(
        $module, 'Insurance Certificates', Array(), 'get_dependents_list', $relatedStudentField->id
);

//Portal Info
$adb->pquery("insert into vtiger_customerportal_fields values (?,?,1)", array(getTabid('InsuranceCertificate'), '{"certificate_number":1,"contact_id":0,"studentid":0,"assigned_user_id":0}'));
$adb->pquery('insert into vtiger_customerportal_tabs values (?,1,15,1,1)', array(getTabid('InsuranceCertificate')));
$adb->pquery("insert into vtiger_customerportal_prefs values (?,'showrelatedinfo',1)", array(getTabid('InsuranceCertificate')));
