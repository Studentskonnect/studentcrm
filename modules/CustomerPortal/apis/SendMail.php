<?php

///Users/salim/NetBeansProjects/vt71/modules/CustomerPortal/apis/SendMail.php
class CustomerPortal_SendMail extends CustomerPortal_API_Abstract {

    protected function processRetrieve(CustomerPortal_API_Request $request) {
        $email = $request->get('toEmail');
        $subject = $request->get('subject');
        $contents = base64_decode($request->get('contents'));
        global $current_user, $HELPDESK_SUPPORT_EMAIL_ID, $HELPDESK_SUPPORT_NAME;
        require_once("modules/Emails/mail.php");
        send_mail('Contacts', $email, $HELPDESK_SUPPORT_NAME, $HELPDESK_SUPPORT_EMAIL_ID, $subject, $contents, '', '', '', '', '', true);
        return true;
    }

    function process(CustomerPortal_API_Request $request) {
        $response = new CustomerPortal_API_Response();
        $current_user = $this->getActiveUser();

        if ($current_user) {
            $record = $this->processRetrieve($request);
            $response->setResult($record);
        }
        return $response;
    }

}

