{strip}
    <div class="col-sm-6 col-lg-6 col-md-6">
        <div class="record-header clearfix">
            <!-- <div class="hidden-sm hidden-xs recordImage bgleads app-{$SELECTED_MENU_CATEGORY}">

                <div class="name"><span><strong><i class="vicon-leads"></i></strong></span></div>
            </div>  -->
			
			<div class="hidden-sm hidden-xs recordImage bgAccounts app-{$SELECTED_MENU_CATEGORY}">  
				{assign var=IMAGE_DETAILS value=$RECORD->getImageDetails()}
				{foreach key=ITER item=IMAGE_INFO from=$IMAGE_DETAILS}
					{if !empty($IMAGE_INFO.path)}
						<img src="{$IMAGE_INFO.path}_{$IMAGE_INFO.orgname}" alt="{$IMAGE_INFO.orgname}" title="{$IMAGE_INFO.orgname}" width="100%" height="100%" align="left"><br>
					{else}
						<img src="{vimage_path('summary_organizations.png')}" class="summaryImg"/>
					{/if}
				{/foreach}
				{if empty($IMAGE_DETAILS)}
					<div class="name"><span><strong><i class="vicon-leads"></i></strong></span></div>
				{/if}
			</div>
			
			
            <div class="recordBasicInfo">
                <div class="info-row">
                    <h4>
                        <span class="recordLabel pushDown" title="{$RECORD->getDisplayValue('salutationtype')}&nbsp;{$RECORD->get('firstname')}&nbsp;{$RECORD->getName()}"> 
                            {if $RECORD->getDisplayValue('salutationtype')}
                                <span class="salutation">  {$RECORD->getDisplayValue('salutationtype')}</span>
                            {/if}
                            {if $RECORD->getDisplayValue('firstname')}
                                <span class="firstname">  {$RECORD->getDisplayValue('firstname')}</span>&nbsp;
                            {/if}
                            {if $RECORD->getDisplayValue('lastname')}
                                <span class="lastname">  {$RECORD->getDisplayValue('lastname')}</span>&nbsp;
                            {/if}
                        </span>
                    </h4>
                </div>
                {include file="DetailViewHeaderFieldsView.tpl"|vtemplate_path:$MODULE}

                <div class="info-row row">
                    {assign var=FIELD_MODEL value=$MODULE_MODEL->getField('email')}
                    <div class="col-lg-7 fieldLabel">
                        <span class="email" title="{vtranslate($FIELD_MODEL->get('label'),$MODULE)} : {$RECORD->get('email')}">
                            {$RECORD->getDisplayValue("email")}
                        </span>
                    </div>
                </div>
                <div class="info-row row">
                    {assign var=FIELD_MODEL value=$MODULE_MODEL->getField('phone')}
                    <div class="col-lg-7 fieldLabel">
                        <span class="phone" title="{vtranslate($FIELD_MODEL->get('label'),$MODULE)} : {$RECORD->get('phone')}">
                            {$RECORD->getDisplayValue("phone")}
                        </span>
                    </div>
                </div>


            </div>
        </div>
    </div>
{/strip}
