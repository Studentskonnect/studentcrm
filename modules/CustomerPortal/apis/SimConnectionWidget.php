<?php

class CustomerPortal_SimConnectionWidget extends CustomerPortal_API_Abstract {

	protected function processRetrieve(CustomerPortal_API_Request $request) {
        $months = array();
        $dateTime = new DateTime();
		$currentMonth=$dateTime->format('m');
        $months[$dateTime->format('M') . $dateTime->format('Y')] = Array('Month' => $dateTime->format('M') . ' ' . $dateTime->format('Y'), 'Amount' => '0.00');
        $dateTime->modify('-1 month');
        while ($dateTime->format('m') != $currentMonth) {
            $months[$dateTime->format('M') . $dateTime->format('Y')] = Array('Month' => $dateTime->format('M') . ' ' . $dateTime->format('Y'), 'Amount' => '0.00');
            $dateTime->modify('-1 month');
        };
        $months = array_reverse($months);

        $db = PearDatabase::getInstance();

        $sql = "SELECT 
                    DATE_FORMAT(B.createdtime, '%b%Y') AS month,
                    SUM(A.amount_paid) as total
                from
                    vtiger_simconnection AS A
                        join
                    vtiger_crmentity AS B ON (B.crmid = A.simconnectionid)
                WHERE
                    A.contact_id = ? and B.deleted = 0
                        AND B.createdtime <= NOW()
                        AND B.createdtime >= date_add(NOW(), interval - 12 month)
                GROUP BY DATE_FORMAT(B.createdtime, '%m-%Y')";


        $sqlResult = $db->pquery($sql, array($this->getActiveCustomer()->id));


        while ($row = $db->fetch_array($sqlResult)) {
            $months[$row['month']]['Amount'] = $row['total'];
        }

        return Array(
            'categories' => array_column($months, 'Month'),
            'data' => array_column($months, 'Amount')
        );
    }

    function process(CustomerPortal_API_Request $request) {
        $response = new CustomerPortal_API_Response();
        $current_user = $this->getActiveUser();

        if ($current_user) {
            $record = $this->processRetrieve($request);
            $response->setResult($record);
        }
        return $response;
    }

}

