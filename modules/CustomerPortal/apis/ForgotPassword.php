<?php
/* +**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.1
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is: vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * ***********************************************************************************/

class CustomerPortal_ForgotPassword extends CustomerPortal_API_Abstract {

	function process(CustomerPortal_API_Request $request) {
		global $adb, $PORTAL_URL, $current_user;
		$userId = $this->getCurrentPortalUser();
		$user = new Users();
		$current_user = $user->retrieveCurrentUserInfoFromFile($userId);

		$response = new CustomerPortal_API_Response();
		$mailid = $request->get('email');
		$current_date = date("Y-m-d");
		$sql = 'SELECT * FROM vtiger_portalinfo
					INNER JOIN vtiger_contactdetails ON vtiger_contactdetails.contactid=vtiger_portalinfo.id
					INNER JOIN vtiger_customerdetails ON vtiger_customerdetails.customerid=vtiger_portalinfo.id
					INNER JOIN vtiger_crmentity ON vtiger_portalinfo.id=vtiger_crmentity.crmid 
						WHERE vtiger_portalinfo.user_name = ? AND vtiger_crmentity.deleted= ?
						AND vtiger_customerdetails.support_start_date <= ?';

		$res = $adb->pquery($sql, array($mailid, '0', $current_date));
		$num_rows = $adb->num_rows($res);

		if ($num_rows > 0) {
			$isActive = $adb->query_result($res, 0, 'isactive');
			$support_end_date = $adb->query_result($res, 0, 'support_end_date');

			if ($isActive && ($support_end_date >= $current_date || $support_end_date == null )) {
				$moduleName = 'Contacts';
				global $HELPDESK_SUPPORT_EMAIL_ID, $HELPDESK_SUPPORT_NAME;
				$user_name = $adb->query_result($res, 0, 'user_name');
				$contactId = $adb->query_result($res, 0, 'id');

				if (!empty($adb->query_result($res, 0, 'cryptmode'))) {
					$password = makeRandomPassword();
					$enc_password = Vtiger_Functions::generateEncryptedPassword($password);

					$sql = 'UPDATE vtiger_portalinfo SET user_password=?, cryptmode=? WHERE id=?';
					$params = array($enc_password, 'CRYPT', $contactId);
					$adb->pquery($sql, $params);
				}
				
				
		
				$portalURL = vtranslate('', $moduleName).'<a href="'.$PORTAL_URL.'" style="font-family:Arial, Helvetica, sans-serif;font-size:13px;">'.vtranslate('click here', $moduleName).'</a>';
				
				
					$contents = "<div style= 'border:1px solid #0095da42;width: 700px;margin: 0 auto;padding: 5px;margin-top: 5%; margin-bottom: 5%;background: #f1f1f1;'>
        <table border='0' cellpadding='0' cellspacing='0' width='100%'>
            <tr>
                <td><table cellpadding='0' cellspacing='0' width='100%' style='border-bottom:1px solid #dadada;'>
                    <tbody>
                   
                      <tr>
                        <td valign='top' align='left'><a href='#'><img alt='' src='https://agentportal.studentskonnect.com/assets/images/logo.png' style='padding-bottom: 0; display: inline !important; height:60px; margin-left: 2%;'></a></td>
                      </tr>
                      
                      <tr>
                    </tr></tbody>
                  </table></td>
              </tr>
   
            <tr>
                <td align='center' style='padding:15px; ' class='section-padding'>
                   
                    <table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 700px;' class='responsive-table'>
                        <tr>
                            <td>
                                <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                                      <tr>
                                        <td>
                                            <table width='100%' border='0' cellspacing='0' cellpadding='0'>
         <tr>
                                                    <td align='center' style='font-size: 14px; text-align:justify; font-family: open sans,sans-serif; color: #444; padding-top: 5px;' class='padding-copy'>
    
        <strong style='font-size: 14px;text-align: left !important; width: 100%;float: left;'>Dear ".$adb->query_result($res, 0, 'firstname').' '.$adb->query_result($res, 0, 'lastname')."</strong><br><br>
        <p>".vtranslate('Here is your Students Konnect Portal login details:', $moduleName)." </p>
        <p><span style='font-weight:700'>".vtranslate('User ID')." : </span> <strong> <a target='_blank'>".$user_name."</a></p>
            <p><span style=' font-weight:700'> Password : </span><font color='#990000'><strong>".$password."</strong></font></p>
            <p><span style=' font-weight:700'>Please click here to login: </span> <a href='#'>".$portalURL."</a></p>
                
        
            
        </td>
         
                                                </tr>
        
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>  
                </td>
            </tr>
            
            <tr>
                <td  align='center' style='padding: 0px 0px;'>
                    <table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' style='max-width: 700px;' class='responsive-table'>
                        <tr>
                            <td align='left' style='font-size: 14px; line-height: 18px; font-family: open sans,sans-serif; color:#444; padding: 0px 15px;'>
                               <strong><em>Regards,</em></strong>
                                <br>
                                <p><em>Students Konnect Team </em></p>
                               
                            </td>
                          
                        </tr>
                        <tr>
                            <td  align='center' style='padding: 0px 0px;'>
                                <table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' style='max-width: 700px;' class='responsive-table'>
                                    <tr>
                                        <td align='left' style='font-size: 10px; line-height: 18px; font-family: open sans,sans-serif; color:#444; padding: 0px 15px;'>
                                          
                                            <p ><strong>DISCLAIMER:</strong> The information in this message is confidential and may be legally privileged. It is intended solely for the addressee. Access to this message by another person is not permitted. If you are not the intended recipient, any disclosure, copying, distribution or any action taken or omitted to be taken in reliance on it, is prohibited and may be unlawful. If you have received this e-mail by mistake, please e-mail the sender (<a href='mailto:'admin@studentskonnect.com'>admin@studentskonnect.com</a>) by replying to this message, and deleting the original and any printout thereof.</p>
                                           <p>NOTE : This is an auto generated email, please do not reply !</p>
                                        </td>
                                      
                                    </tr>
                        </tr>
                    </table>
                    
                </td>
            </tr>
        </table>
    </div>";
				
				
				// <table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
				// <strong style="font-size: 14px;text-align: left !important; width: 100%;float: left;">Dear '.$adb->query_result($res, 0, 'firstname')." ".$adb->query_result($res, 0, 'lastname').'</strong><br></td></tr><tr>
				// 				<td>'.vtranslate('Here is your self service portal login details:', $moduleName).'</td></tr><tr><td align="center"><br><table style="border:2px solid rgb(180,180,179);background-color:rgb(226,226,225);" cellspacing="0" cellpadding="10" border="0" width="75%"><tr>
				// 				<td><br>'.vtranslate('User ID').' : <font color="#990000"><strong><a target="_blank">'.$user_name.'</a></strong></font></td></tr><tr>
				// 				<td>'.vtranslate('Password').' : <font color="#990000"><strong>'.$password.'</strong></font></td></tr><tr>
				// 				<td align="center"><strong>'.$portalURL.'</strong></td>
				// 				</tr></table><br></td></tr><tr><td><strong>NOTE: </strong>'.vtranslate('We suggest you to change your password after logging in first time').'.<br>
				// 			</td></tr></table>';

				// $adb->println("umeshkumarsaroj");
				// $adb->println($contents);
				$subject = 'Reset password';
				$subject = decode_html(getMergedDescription($subject, $contactId, $moduleName));

				$sql = 'UPDATE vtiger_student SET agent_password=? WHERE agent_email=?';
					$params = array($password, $user_name);
					$adb->pquery($sql, $params);
			

					$mailStatus = send_mail($moduleName, $user_name, $HELPDESK_SUPPORT_NAME, $HELPDESK_SUPPORT_EMAIL_ID, $subject, $contents, '', '', '', '', '', true);
				$ret_msg = vtranslate('LBL_MAIL_COULDNOT_SENT', 'HelpDesk');
				if ($mailStatus) {
					$ret_msg = vtranslate('LBL_MAIL_SENT', 'HelpDesk');
				}
				$response->setResult($ret_msg);
			} else if ($isActive && $support_end_date <= $current_date) {
				throw new Exception('Access to the portal was disabled on '.$support_end_date, 1413);
			} else if ($isActive == 0) {
				throw new Exception('Portal access has not been enabled for this account.', 1414);
			}
		} else {
			$response->setError('1412', 'Invalid email');
		}
		return $response;
	}

	function authenticatePortalUser($username, $password) {
		// always return true
		return true;
	}

	public function getCurrentPortalUser() {
		$db = PearDatabase::getInstance();

		$result = $db->pquery("SELECT prefvalue FROM vtiger_customerportal_prefs WHERE prefkey = 'userid' AND tabid = 0", array());
		if ($db->num_rows($result)) {
			return $db->query_result($result, 0, 'prefvalue');
		}
		return false;
	}

}
