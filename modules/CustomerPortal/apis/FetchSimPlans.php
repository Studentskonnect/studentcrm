<?php

/* +**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.1
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is: vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * ********************************************************************************** */

class CustomerPortal_FetchSimPlans extends CustomerPortal_API_Abstract {

    protected function processRetrieve(CustomerPortal_API_Request $request) {
        $db = PearDatabase::getInstance();
        $sql = "SELECT    CONCAT(
                            (
                                   SELECT id
                                   FROM   vtiger_ws_entity
                                   WHERE  name = 'SimPlan'), 'x', a.simplanid) AS id,
                            a.plan_code,
                            c.country_listid AS country_list_id,
                            a.country_list AS country,
                            c.country_code   AS country_code,
                            b.description,
                            a.cost,
                            a.Validity
                  FROM      vtiger_simplan   AS a
                  JOIN      vtiger_crmentity AS b ON (a.simplanid = b.crmid)
                  LEFT JOIN vtiger_country_list AS c ON (a.country_list = c.country_list)
                  WHERE     b.deleted = 0";


        $sqlResult = $db->pquery($sql, array());
        $result = Array();
        $plans = Array();
        while ($row = $db->fetch_array($sqlResult)) {

            $plans[$row['country_list_id']][] = Array(
                'id' => $row['id'],
                'plan_code' => $row['plan_code'],
                'cost' => $row['cost'],
		'validity' => $row['validity'],
		'plan_detail' =>  explode(PHP_EOL, $row['description']),
		//'plan_detail' => array_map(ucfirst,explode(PHP_EOL, $row['description']))
            );
            $result[$row['country_list_id']] = Array(
                'id' => $row['country_list_id'],
                'country_code' => $row['country_code'],
                'name' => $row['country'],
                'plans' => $plans[$row['country_list_id']],
            );
        }
        return array_values($result);
    }

    function process(CustomerPortal_API_Request $request) {
        $response = new CustomerPortal_API_Response();
        $current_user = $this->getActiveUser();

        if ($current_user) {
            $record = $this->processRetrieve($request);
            $response->setResult($record);
        }
        return $response;
    }

}
