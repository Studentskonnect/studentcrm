<?php


//This is the sharing access privilege file
$defaultOrgSharingPermission=array('2'=>2,'4'=>0,'6'=>0,'7'=>2,'9'=>3,'13'=>2,'16'=>3,'20'=>2,'21'=>2,'22'=>2,'23'=>2,'26'=>2,'8'=>2,'14'=>2,'32'=>3,'33'=>2,'37'=>2,'39'=>2,'40'=>2,'41'=>2,'42'=>0,'44'=>0,'45'=>2,'18'=>2,'10'=>0,'50'=>2,'51'=>0,'52'=>0,'53'=>0,'54'=>0,'55'=>0,'56'=>3,'57'=>2,'58'=>2,'59'=>2,'60'=>2,);

$related_module_share=array(2=>array(6,),13=>array(6,),20=>array(6,2,),22=>array(6,2,20,),23=>array(6,22,),);

$Leads_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Leads_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Leads_Emails_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Leads_Emails_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Contacts_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Contacts_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_Potentials_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_Potentials_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_HelpDesk_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_HelpDesk_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_Emails_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_Emails_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_Quotes_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_Quotes_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_SalesOrder_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_SalesOrder_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_Invoice_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_Invoice_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Potentials_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Potentials_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Potentials_Quotes_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Potentials_Quotes_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Potentials_SalesOrder_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Potentials_SalesOrder_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$HelpDesk_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$HelpDesk_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Emails_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Emails_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Campaigns_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Campaigns_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Quotes_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Quotes_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Quotes_SalesOrder_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Quotes_SalesOrder_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$PurchaseOrder_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$PurchaseOrder_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$SalesOrder_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$SalesOrder_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$SalesOrder_Invoice_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$SalesOrder_Invoice_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Invoice_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Invoice_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Documents_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Documents_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Services_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Services_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$SMSNotifier_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$SMSNotifier_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$ModComments_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$ModComments_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Student_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Student_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$AirlineTicket_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$AirlineTicket_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$InsurancePlan_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$InsurancePlan_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$InsuranceCertificate_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$InsuranceCertificate_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$SimConnection_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$SimConnection_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$SimPlan_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$SimPlan_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accommodation_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accommodation_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$CountryResume_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$CountryResume_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$ResumeInfo_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$ResumeInfo_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$StudentResumeProfile_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$StudentResumeProfile_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

?>