<?php

//Users/salim/NetBeansProjects/vt71/AirlineTicket.php

$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = Vtiger_Module::getInstance('AirlineTicket');
if (!$module) {
    $module = new Vtiger_Module();
    $module->name = 'AirlineTicket';
    $module->parent = 'Sales';
    $module->save();
    $module->initTables();
    $module->initWebservice();
}
/*
 * 
 * Blocks for Architects Module
 * 
 */
//     info block
$infoBlock = Vtiger_Block::getInstance('LBL_TICKET_DETAILS', $module);
if (!$infoBlock) {
    $infoBlock = new Vtiger_Block();
    $infoBlock->label = 'LBL_TICKET_DETAILS';
    $module->addBlock($infoBlock);
}

//Description Details
$descriptionBlock = Vtiger_Block::getInstance('LBL_DESCRIPTION_INFORMATION', $module);
if (!$descriptionBlock) {
    $descriptionBlock = new Vtiger_Block();
    $descriptionBlock->label = 'LBL_DESCRIPTION_INFORMATION';
    $module->addBlock($descriptionBlock);
}

$pnrnoField = Vtiger_Field::getInstance('pnrno', $module);
if (!$pnrnoField) {
    $pnrnoField = new Vtiger_Field();
    $pnrnoField->name = 'pnrno';
    $pnrnoField->label = 'PNR Number';
    $pnrnoField->table = $module->basetable;
    $pnrnoField->column = 'pnrno';
    $pnrnoField->columntype = 'VARCHAR(128)';
    $pnrnoField->uitype = 255;
    $pnrnoField->typeofdata = 'V~M';
    $infoBlock->addField($pnrnoField);
    /** Creates the field and adds to block */
    $module->setEntityIdentifier($pnrnoField);
}

/*
 * Party
 */
$relatedToField = Vtiger_Field::getInstance('contact_id', $module);
if (!$relatedToField) {
    $relatedToField = new Vtiger_Field();
    $relatedToField->name = 'contact_id';
    $relatedToField->label = 'Agent';
    $relatedToField->table = $module->basetable;
    $relatedToField->columntype = 'INT(11)';
    $relatedToField->uitype = 10;
    $relatedToField->typeofdata = 'V~M';
    $infoBlock->addField($relatedToField);
    $relatedToField->setRelatedModules(array('Contacts'));
}
/*
 * Party
 */
$relatedStudentField = Vtiger_Field::getInstance('studentid', $module);
if (!$relatedStudentField) {
    $relatedStudentField = new Vtiger_Field();
    $relatedStudentField->name = 'studentid';
    $relatedStudentField->label = 'Student';
    $relatedStudentField->table = $module->basetable;
    $relatedStudentField->columntype = 'INT(11)';
    $relatedStudentField->uitype = 10;
    $relatedStudentField->typeofdata = 'V~M';
    $infoBlock->addField($relatedStudentField);
    $relatedStudentField->setRelatedModules(array('Student'));
}


/*
 * Field For Assigned To
 */
$assignedToField = Vtiger_Field::getInstance('assigned_user_id', $module);
if (!$assignedToField) {
    $assignedToField = new Vtiger_Field();
    $assignedToField->name = 'assigned_user_id';
    $assignedToField->label = 'Assigned To';
    $assignedToField->table = 'vtiger_crmentity';
    $assignedToField->column = 'smownerid';
    $assignedToField->uitype = 53;
    $assignedToField->typeofdata = 'V~M';
    $infoBlock->addField($assignedToField);
}

$createdTimeField = Vtiger_Field::getInstance('createdtime', $module);
if (!$createdTimeField) {
    $createdTimeField = new Vtiger_Field();
    $createdTimeField->name = 'createdtime';
    $createdTimeField->label = 'Created Time';
    $createdTimeField->table = 'vtiger_crmentity';
    $createdTimeField->column = 'createdtime';
    $createdTimeField->uitype = 70;
    $createdTimeField->typeofdata = 'T~O';
    $createdTimeField->displaytype = 2;
    $infoBlock->addField($createdTimeField);
}
/*
 * Modified Time
 */
$modifiedTimeField = Vtiger_Field::getInstance('modifiedtime', $module);
if (!$modifiedTimeField) {
    $modifiedTimeField = new Vtiger_Field();
    $modifiedTimeField->name = 'modifiedtime';
    $modifiedTimeField->label = 'Modified Time';
    $modifiedTimeField->table = 'vtiger_crmentity';
    $modifiedTimeField->column = 'modifiedtime';
    $modifiedTimeField->uitype = 70;
    $modifiedTimeField->typeofdata = 'T~O';
    $modifiedTimeField->displaytype = 2;
    $infoBlock->addField($modifiedTimeField);
}

$descriptionField = Vtiger_Field::getInstance('ticketdata', $module);
if (!$descriptionField) {
    $descriptionField = new Vtiger_Field();
    $descriptionField->name = 'ticketdata';
    $descriptionField->label = 'Ticket Data';
    $descriptionField->table = $module->basetable;
    $descriptionField->columntype = 'MEDIUMTEXT';
    $descriptionField->uitype = 19;
    $descriptionField->typeofdata = 'V~O';
    $descriptionBlock->addField($descriptionField);
    /** Creates the field and adds to block */
}
$allFilter = Vtiger_Filter::getInstance('All', $module);
if (!$allFilter) {
    $allFilter = new Vtiger_Filter();
    $allFilter->name = 'All';
    $allFilter->isdefault = true;
    $module->addFilter($allFilter);
// Add fields to the filter created
    $allFilter->addField($pnrnoField)
            ->addField($pnrnoField, 1)
            ->addField($relatedToField, 2)
            ->addField($relatedStudentField, 3)
            ->addField($createdTimeField, 4);
           
}

global $adb;

$adb->pquery('INSERT INTO vtiger_app2tab(tabid,appname,sequence,visible) values(?,?,?,?)', array(getTabid('AirlineTicket'), 'SALES', 30, 1));
$adb->pquery('CREATE TABLE `vtiger_airlineticket_user_field` (
  `recordid` int(25) NOT NULL,
  `userid` int(25) NOT NULL,
  `starred` varchar(100) DEFAULT NULL,
  KEY `fk_airlineticketid_vtiger_airlineticket_user_field` (`recordid`),
  CONSTRAINT `fk_airlineticketid_vtiger_airlineticket_user_field` FOREIGN KEY (`recordid`) REFERENCES `vtiger_airlineticket` (`airlineticketid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8',Array());


/** Set sharing access of this module */
$module->setDefaultSharing('Private');
/** Enable and Disable available tools */
//$module->enableTools(Array('Import'));
$module->disableTools('Merge');

$contactsModule = Vtiger_Module::getInstance('Contacts');
$contactsModule->setRelatedList(
        $module, 'Airline Tickets', Array(), 'get_dependents_list', $relatedToField->id
);

$studentModule = Vtiger_Module::getInstance('Student');
$studentModule->setRelatedList(
        $module, 'Airline Tickets', Array(), 'get_dependents_list', $relatedStudentField->id
);

//Portal Info
$adb->pquery("insert into vtiger_customerportal_fields values (?,?,1)", array(getTabid('AirlineTicket'), '{"pnrno":1,"contact_id":0,"studentid":0,"assigned_user_id":0,"ticketdata":0}'));
$adb->pquery('insert into vtiger_customerportal_tabs values (?,1,15,1,1)', array(getTabid('AirlineTicket')));
$adb->pquery("insert into vtiger_customerportal_prefs values (?,'showrelatedinfo',1)", array(getTabid('AirlineTicket')));
