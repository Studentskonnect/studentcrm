<?php
/*+**********************************************************************************
 * Created by : Salim
 * Email : salimcmd@gmail.com, salim@oztro.com
 ************************************************************************************/
$languageStrings = array(
// Basic Strings
    'AirlineTicket' => 'Airline Tickets',
    'SINGLE_AirlineTicket' => 'Airline Ticket',
    'LBL_ADD_RECORD' => 'Add Airline Ticket',
    'LBL_RECORDS_LIST' => 'Airline Ticket List',
    // Blocks
    'LBL_TICKET_DETAILS' => 'Airline Ticket Details',
    //Field Labels
    "Phone" => "Phone",
    "Email" => "Email",
    "Occupation" => "Occupation",
    "Position" => "Position",
    "Website" => "Website",
    "Address" => "Address",
    "State" => "State",
    "Location Name" => "City",
    "Postal code" => "Postal code",
    "Area of Speciality" => "Area of Speciality",
    "Assigned To" => "Assigned To",
    "Created Time" => "Created Time",
    "Modified Time" => "Modified Time",
    "Description" => "Description"
);

$jsLanguageStrings = array();
