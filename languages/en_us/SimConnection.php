<?php
/*+**********************************************************************************
 * Created by : Salim
 * Email : salimcmd@gmail.com, salim@oztro.com
 ************************************************************************************/
$languageStrings = array(
// Basic Strings
    'SimConnection' => 'Sim Connections',
    'SINGLE_SimConnection' => 'Sim Connection',
    'LBL_ADD_RECORD' => 'Add Sim Connection',
    'LBL_RECORDS_LIST' => 'Sim Connection List',
    // Blocks
    'LBL_CONNECTION_DETAILS' => 'Sim Connection Details',
    'LBL_DESCRIPTION_INFORMATION' => 'Plan  Details',
    //Field Labels
    "Phone" => "Phone",
    "Email" => "Email",
    "Occupation" => "Occupation",
    "Position" => "Position",
    "Website" => "Website",
    "Address" => "Address",
    "State" => "State",
    "Location Name" => "City",
    "Postal code" => "Postal code",
    "Area of Speciality" => "Area of Speciality",
    "Assigned To" => "Assigned To",
    "Created Time" => "Created Time",
    "Modified Time" => "Modified Time",
    "Description" => "Description"
);

$jsLanguageStrings = array();
