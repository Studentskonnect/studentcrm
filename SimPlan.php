<?php

//Users/salim/NetBeansPlans/vt71/SimPlan.php

$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = Vtiger_Module::getInstance('SimPlan');
if (!$module) {
    $module = new Vtiger_Module();
    $module->name = 'SimPlan';
    $module->parent = 'Sales';
    $module->save();
    $module->initTables();
    $module->initWebservice();
}

//     info block
$infoBlock = Vtiger_Block::getInstance('LBL_PLAN_DETAILS', $module);
if (!$infoBlock) {
    $infoBlock = new Vtiger_Block();
    $infoBlock->label = 'LBL_PLAN_DETAILS';
    $module->addBlock($infoBlock);
}

//Description Details
$descriptionBlock = Vtiger_Block::getInstance('LBL_DESCRIPTION_INFORMATION', $module);
if (!$descriptionBlock) {
    $descriptionBlock = new Vtiger_Block();
    $descriptionBlock->label = 'LBL_DESCRIPTION_INFORMATION';
    $module->addBlock($descriptionBlock);
}

$pnrnoField = Vtiger_Field::getInstance('plan_code', $module);
if (!$pnrnoField) {
    $pnrnoField = new Vtiger_Field();
    $pnrnoField->name = 'plan_code';
    $pnrnoField->label = 'Plan Code';
    $pnrnoField->table = $module->basetable;
    $pnrnoField->column = 'plan_code';
    $pnrnoField->columntype = 'VARCHAR(128)';
    $pnrnoField->uitype = 1;
    $pnrnoField->typeofdata = 'V~M';
    $infoBlock->addField($pnrnoField);
    /** Creates the field and adds to block */
    $module->setEntityIdentifier($pnrnoField);
}


$countryField = Vtiger_Field::getInstance('country_list', $module);
if (!$countryField) {
    $countryField = new Vtiger_Field();
    $countryField->name = 'country_list';
    $countryField->label = 'Country';
    $countryField->column = 'country_list';
    $countryField->columntype = 'VARCHAR(100)';
    $countryField->uitype = 16;
    $countryField->typeofdata = 'V~O';
    $infoBlock->addField($countryField);
    $countryField->setPicklistValues(array('Afghanistan'));
}

$rateField = Vtiger_Field::getInstance('cost', $module);
if (!$rateField) {
    $rateField = new Vtiger_Field();
    $rateField->name = 'cost';
    $rateField->label = 'Cost';
    $rateField->table = $module->basetable;
    $rateField->column = 'cost';
    $rateField->columntype = 'INT(12)';
    $rateField->uitype = 71;
    $rateField->typeofdata = 'V~M';
    $infoBlock->addField($rateField);
}

$ValidityField = Vtiger_Field::getInstance('Validity', $module);
if (!$ValidityField) {
    $ValidityField = new Vtiger_Field();
    $ValidityField->name = 'Validity';
    $ValidityField->label = 'Validity (In Days)';
    $ValidityField->table = $module->basetable;
    $ValidityField->column = 'Validity';
    $ValidityField->columntype = 'VARCHAR(32)';
    $ValidityField->uitype = 1;
    $ValidityField->typeofdata = 'V~O';
    $infoBlock->addField($ValidityField);
}
/*
 * Field For Assigned To
 */
$assignedToField = Vtiger_Field::getInstance('assigned_user_id', $module);
if (!$assignedToField) {
    $assignedToField = new Vtiger_Field();
    $assignedToField->name = 'assigned_user_id';
    $assignedToField->label = 'Assigned To';
    $assignedToField->table = 'vtiger_crmentity';
    $assignedToField->column = 'smownerid';
    $assignedToField->uitype = 53;
    $assignedToField->typeofdata = 'V~M';
    $infoBlock->addField($assignedToField);
}

$createdTimeField = Vtiger_Field::getInstance('createdtime', $module);
if (!$createdTimeField) {
    $createdTimeField = new Vtiger_Field();
    $createdTimeField->name = 'createdtime';
    $createdTimeField->label = 'Created Time';
    $createdTimeField->table = 'vtiger_crmentity';
    $createdTimeField->column = 'createdtime';
    $createdTimeField->uitype = 70;
    $createdTimeField->typeofdata = 'T~O';
    $createdTimeField->displaytype = 2;
    $infoBlock->addField($createdTimeField);
}
/*
 * Modified Time
 */
$modifiedTimeField = Vtiger_Field::getInstance('modifiedtime', $module);
if (!$modifiedTimeField) {
    $modifiedTimeField = new Vtiger_Field();
    $modifiedTimeField->name = 'modifiedtime';
    $modifiedTimeField->label = 'Modified Time';
    $modifiedTimeField->table = 'vtiger_crmentity';
    $modifiedTimeField->column = 'modifiedtime';
    $modifiedTimeField->uitype = 70;
    $modifiedTimeField->typeofdata = 'T~O';
    $modifiedTimeField->displaytype = 2;
    $infoBlock->addField($modifiedTimeField);
}

$descriptionField = Vtiger_Field::getInstance('description', $module);
if (!$descriptionField) {
    $descriptionField = new Vtiger_Field();
    $descriptionField->name = 'description';
    $descriptionField->label = 'Plan Detail';
    $descriptionField->table = 'vtiger_crmentity';
    $descriptionField->column = 'description';
    $descriptionField->uitype = 19;
    $descriptionField->typeofdata = 'V~O';
    $descriptionBlock->addField($descriptionField);
    /** Creates the field and adds to block */
}


$allFilter = Vtiger_Filter::getInstance('All', $module);
if (!$allFilter) {
    $allFilter = new Vtiger_Filter();
    $allFilter->name = 'All';
    $allFilter->isdefault = true;
    $module->addFilter($allFilter);
// Add fields to the filter created
    $allFilter->addField($pnrnoField)
            ->addField($countryField, 1)
            ->addField($rateField, 2)
            ->addField($ValidityField, 3)
            ->addField($descriptionField, 4);
}


/** Set sharing access of this module */
$module->setDefaultSharing('Private');
/** Enable and Disable available tools */
$module->enableTools(Array('Import'));
$module->disableTools('Merge');
global $adb;

$adb->pquery('INSERT INTO vtiger_app2tab(tabid,appname,sequence,visible) values(?,?,?,?)', array(getTabid('SimPlan'), 'SALES', 30, 1));
$adb->pquery('CREATE TABLE `vtiger_simplan_user_field` (
  `recordid` int(25) NOT NULL,
  `userid` int(25) NOT NULL,
  `starred` varchar(100) DEFAULT NULL,
  KEY `fk_simplanid_vtiger_simplan_user_field` (`recordid`),
  CONSTRAINT `fk_simplanid_vtiger_simplan_user_field` FOREIGN KEY (`recordid`) REFERENCES `vtiger_simplan` (`simplanid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8', Array());
