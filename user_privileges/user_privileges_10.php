<?php


//This is the access privilege file
$is_admin=false;

$current_user_roles='H7';

$current_user_parent_role_seq='H1::H2::H3::H7';

$current_user_profiles=array(7,);

$profileGlobalPermission=array('1'=>1,'2'=>1,);

$profileTabsPermission=array('1'=>0,'2'=>1,'4'=>0,'6'=>0,'7'=>0,'8'=>1,'9'=>0,'10'=>0,'13'=>1,'16'=>0,'23'=>1,'25'=>0,'35'=>1,'37'=>1,'42'=>1,'44'=>0,'46'=>1,'48'=>0,'50'=>0,'51'=>0,'52'=>1,'53'=>0,'54'=>0,'55'=>1,'28'=>0,'3'=>0,);

$profileActionPermission=array(2=>array(0=>1,1=>1,2=>1,4=>1,7=>1,),4=>array(0=>0,1=>0,2=>1,4=>0,7=>0,),6=>array(0=>0,1=>0,2=>1,4=>0,7=>0,),7=>array(0=>0,1=>0,2=>1,4=>0,7=>0,),8=>array(0=>0,1=>0,2=>0,4=>0,7=>0,),9=>array(0=>0,1=>0,2=>0,4=>0,7=>0,),13=>array(0=>1,1=>1,2=>1,4=>1,7=>1,),16=>array(0=>0,1=>0,2=>0,4=>0,7=>0,),23=>array(0=>1,1=>1,2=>1,4=>1,7=>1,),25=>array(0=>1,1=>0,2=>0,4=>0,7=>0,),37=>array(0=>1,1=>1,2=>1,4=>1,7=>1,),42=>array(0=>0,1=>0,2=>1,4=>0,7=>0,),44=>array(0=>0,1=>0,2=>0,4=>0,7=>0,),50=>array(0=>1,1=>1,2=>1,4=>0,7=>1,),51=>array(0=>1,1=>1,2=>1,4=>0,7=>1,5=>0,6=>0,8=>1,),52=>array(0=>1,1=>1,2=>1,4=>0,7=>1,),53=>array(0=>1,1=>1,2=>1,4=>0,7=>1,5=>0,6=>0,8=>1,),54=>array(0=>1,1=>1,2=>1,4=>0,7=>1,5=>0,6=>0,8=>1,),55=>array(0=>1,1=>1,2=>1,4=>0,7=>1,),);

$current_user_groups=array(3,4,12,);

$subordinate_roles=array();

$parent_roles=array('H1','H2','H3',);

$subordinate_roles_users=array();

$user_info=array('user_name'=>'saurabh@studentskonnect.com','is_admin'=>'off','user_password'=>'$2y$10$8aRv4IDyNLRcxbHeU6EqAewKZwND.eu37O730FDsCFdSvudPD.l0W','confirm_password'=>'$2y$10$DvfHREKXHw9aKHB/UYEnHu3ispN.4P.bjnw9Tu3vMCVFQBOsbPa9O','first_name'=>'Saurabh','last_name'=>'Vashisth','roleid'=>'H7','email1'=>'saurabh@studentskonnect.com','status'=>'Active','activity_view'=>'Today','lead_view'=>'Today','hour_format'=>'12','end_hour'=>'','start_hour'=>'09:00','is_owner'=>'','title'=>'','phone_work'=>'','department'=>'','phone_mobile'=>'','reports_to_id'=>'','phone_other'=>'','email2'=>'','phone_fax'=>'','secondaryemail'=>'','phone_home'=>'','date_format'=>'dd-mm-yyyy','signature'=>'','description'=>'','address_street'=>'','address_city'=>'','address_state'=>'','address_postalcode'=>'','address_country'=>'','accesskey'=>'F5qHcToHLieaM8vZ','time_zone'=>'Asia/Kolkata','currency_id'=>'1','currency_grouping_pattern'=>'123,456,789','currency_decimal_separator'=>'.','currency_grouping_separator'=>',','currency_symbol_placement'=>'$1.0','imagename'=>'','internal_mailer'=>'0','theme'=>'softed','language'=>'en_us','reminder_interval'=>'','phone_crm_extension'=>'','no_of_currency_decimals'=>'2','truncate_trailing_zeros'=>'0','dayoftheweek'=>'Monday','callduration'=>'5','othereventduration'=>'5','calendarsharedtype'=>'public','default_record_view'=>'Summary','leftpanelhide'=>'1','rowheight'=>'medium','defaulteventstatus'=>'','defaultactivitytype'=>'','hidecompletedevents'=>'0','defaultcalendarview'=>'','currency_name'=>'India, Rupees','currency_code'=>'INR','currency_symbol'=>'₹','conv_rate'=>'1.00000','record_id'=>'','record_module'=>'','id'=>'10');
?>