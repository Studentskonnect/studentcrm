<?php

$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = Vtiger_Module::getInstance('Contacts');
$infoBlock = Vtiger_Block::getInstance('LBL_CUSTOMER_PORTAL_INFORMATION', $module);

$ReferenceField = Vtiger_Field::getInstance('flyremit_key', $module);
if (!$ReferenceField) {
    $ReferenceField = new Vtiger_Field();
    $ReferenceField->name = 'flyremit_key';
    $ReferenceField->label = 'FlyRemit Key';
    $ReferenceField->table = $module->basetable;
    $ReferenceField->column = 'flyremit_key';
    $ReferenceField->columntype = 'VARCHAR(128)';
    $ReferenceField->uitype = 1;
    $ReferenceField->displaytype = 2;
    $ReferenceField->typeofdata = 'V~O';
    
    $infoBlock->addField($ReferenceField);
}
