<?php
/*+**********************************************************************************
 * Created by : Salim
 * Email : salimcmd@gmail.com, salim@oztro.com
 ************************************************************************************/
$languageStrings = array(
// Basic Strings
    'Student' => 'Students',
    'SINGLE_Student' => 'Student',
    'LBL_ADD_RECORD' => 'Add Student',
    'LBL_RECORDS_LIST' => 'Student List',
    // Blocks
    'LBL_STUDENT_DETAILS' => 'Student Details',
    //Field Labels
    "Phone" => "Phone",
    "Email" => "Email",
    "Occupation" => "Occupation",
    "Position" => "Position",
    "Website" => "Website",
    "Address" => "Address",
    "State" => "State",
    "Location Name" => "City",
    "Postal code" => "Postal code",
    "Area of Speciality" => "Area of Speciality",
    "Assigned To" => "Assigned To",
    "Created Time" => "Created Time",
    "Modified Time" => "Modified Time",
    "Description" => "Description"
);

$jsLanguageStrings = array();
