<?php

//Users/salim/NetBeansProjects/vt71/StudentTask.php

$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = Vtiger_Module::getInstance('StudentTask');
$infoBlock = Vtiger_Block::getInstance('Task Details', $module);
$relatedStudentField = Vtiger_Field::getInstance('studentid', $module);
if (!$relatedStudentField) {
    $relatedStudentField = new Vtiger_Field();
    $relatedStudentField->name = 'studentid';
    $relatedStudentField->label = 'Student';
    $relatedStudentField->table = $module->basetable;
    $relatedStudentField->columntype = 'INT(11)';
    $relatedStudentField->uitype = 10;
    $relatedStudentField->typeofdata = 'V~M';
    $infoBlock->addField($relatedStudentField);
    $relatedStudentField->setRelatedModules(array('Student'));
}

$studentModule = Vtiger_Module::getInstance('Student');
$studentModule->setRelatedList(
        $module, 'Airline Tickets', Array(), 'get_dependents_list', $relatedStudentField->id
);

