<?php

class CustomerPortal_AirlineTicketReport extends CustomerPortal_API_Abstract {

    protected function processRetrieve(CustomerPortal_API_Request $request) {
	$db = PearDatabase::getInstance();
	$studentid = explode('x',$request->get('studentId')) ; 
	$studentSql = '';
	if(count($studentid) > 1) {
		$studentSql =  " AND A.studentid = ". $studentid[1]."  ";
	}
        $sql = "select
                    pnrno 'pnr_number',
                    F.accountname 'organisation',
                    A.firstname 'first_name',
                    A.middlename 'middle_name',
                    A.lastname 'last_name',
                    DATE_FORMAT(E.cf_868,'%d-%b-%Y') 'dob',
                    A.phone,
                    A.email,
                    E.cf_975 'passport',
                    C.cf_989 'flight_from',
                    C.cf_991 'flight_to',
                    DATE_FORMAT(CONVERT_TZ(B.createdtime,'+00:00','+05:30'), '%d-%b-%Y') 'purchase_date',
                    DATE_FORMAT(CONVERT_TZ(B.createdtime,'+00:00','+05:30'), '%H:%i') 'purchase_time',
                    round(C.cf_993,2) 'amount'
                from
                    vtiger_airlineticket AS A
                        join
                    vtiger_crmentity AS B ON (B.crmid = A.airlineticketid)
                        JOIN
                    vtiger_airlineticketcf AS C ON (A.airlineticketid = C.airlineticketid)
                        LEFT JOIN
                    vtiger_student AS D ON (A.studentid = D.studentid)
                        LEFT JOIN
                    vtiger_studentcf AS E ON (A.studentid = E.studentid)
                        LEFT JOIN
                    vtiger_account AS F ON (A.account_id = F.accountid)
                where
                   A.contact_id = ? $studentSql and  B.deleted = 0 AND (B.createdtime BETWEEN ? AND ?) order by B.createdtime DESC";

        $sqlResult = $db->pquery($sql, array($this->getActiveCustomer()->id,$request->get('fromDate').' 00:00:00',$request->get('toDate').' 23.59:59'));
        $result = Array();

        while ($row = $db->fetch_array($sqlResult)) {
            $result[] = $row;
        }
        return $result;
    }

    function process(CustomerPortal_API_Request $request) {
        $response = new CustomerPortal_API_Response();
        $current_user = $this->getActiveUser();

        if ($current_user) {
            $record = $this->processRetrieve($request);
            $response->setResult($record);
        }
        return $response;
    }

}
