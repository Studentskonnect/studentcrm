<?php

class CustomerPortal_InsuranceCertificateReport extends CustomerPortal_API_Abstract {

    protected function processRetrieve(CustomerPortal_API_Request $request) {
	$db = PearDatabase::getInstance();
	$studentid = explode('x',$request->get('studentId')) ;
        $studentSql = '';
        if(count($studentid) > 1) {
                $studentSql =  " AND A.studentid = ". $studentid[1]."  ";
        }

	$sql = "select 
		A.insurancecertificateid,
                A.certificate_number 'certificate_number',
                F.accountname 'organisation',
                A.firstname 'first_name',
                E.cf_934 'middle_name',
		A.lastname 'last_name',
		DATE_FORMAT(E.cf_868,'%d-%b-%Y') 'dob',
                A.phone,
                A.email,
                E.cf_975 'passport',
                G.plan 'plan_name',
                DATE_FORMAT(C.cf_998,'%d-%b-%Y') 'starting_date',
                DATE_FORMAT(C.cf_1000,'%d-%b-%Y') 'finish_date',
		DATE_FORMAT(B.createdtime, '%d-%b-%Y') 'purchase_date',
		DATE_FORMAT(CONVERT_TZ(B.createdtime,'+00:00','+05:30'), '%H:%i') 'purchase_time',
                C.cf_1002 'amount'
            from
                vtiger_insurancecertificate AS A
                    join
                vtiger_crmentity AS B ON (B.crmid = A.insurancecertificateid)
                    JOIN
                vtiger_insurancecertificatecf AS C ON (A.insurancecertificateid = C.insurancecertificateid)
                    LEFT JOIN
                vtiger_student AS D ON (A.studentid = D.studentid)
                    LEFT JOIN
                vtiger_studentcf AS E ON (A.studentid = E.studentid)
                    LEFT JOIN
		vtiger_account AS F ON (A.account_id = F.accountid)
		    LEFT JOIN
		vtiger_insuranceplan_plan AS G ON (C.cf_996 = G.plan_code)
            where
		A.contact_id = ? $studentSql  and  B.deleted = 0 AND (B.createdtime BETWEEN ? AND ?)  order by B.createdtime DESC";

         $sqlResult = $db->pquery($sql, array($this->getActiveCustomer()->id,$request->get('fromDate').' 00:00:00',$request->get('toDate').' 23.59:59' )); 
        $result = Array();

        while ($row = $db->fetch_array($sqlResult)) {
            $result[] = $row;
        }

        return $result;
    }

    function process(CustomerPortal_API_Request $request) {
        $response = new CustomerPortal_API_Response();
        $current_user = $this->getActiveUser();

        if ($current_user) {
            $record = $this->processRetrieve($request);
            $response->setResult($record);
        }
        return $response;
    }

}
