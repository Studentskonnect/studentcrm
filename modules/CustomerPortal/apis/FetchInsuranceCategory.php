<?php

/* +**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.1
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is: vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * ********************************************************************************** */

class CustomerPortal_FetchInsuranceCategory extends CustomerPortal_API_Abstract {

    protected function processRetrieve(CustomerPortal_API_Request $request) {
        $db = PearDatabase::getInstance();
        $sql = "select * from vtiger_insuranceplan_category";


        $sqlResult = $db->pquery($sql, array());
        $result = Array();
        $plans = Array();
        while ($row = $db->fetch_array($sqlResult)) {
            
            $result[] = Array(
                'category_code' => $row['category_code'],
                'category' => $row['category'],
            );
        }
        return array_values($result);
    }

    function process(CustomerPortal_API_Request $request) {
        $response = new CustomerPortal_API_Response();
        $current_user = $this->getActiveUser();

        if ($current_user) {
            $record = $this->processRetrieve($request);
            $response->setResult($record);
        }
        return $response;
    }

}
