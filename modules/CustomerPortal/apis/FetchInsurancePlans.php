<?php

/* +**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.1
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is: vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * ********************************************************************************** */

class CustomerPortal_FetchInsurancePlans extends CustomerPortal_API_Abstract {

    private $plancodearray=Array();
    protected function processRetrieve(CustomerPortal_API_Request $request) {
        $category_code = $request->get('category_code');
        $age_limit = $request->get('age');
        $country_code = $request->get('country_code');
        $day_limit = $request->get('days');
        $benefit_limit = $request->get('benefit_limit');
        $db = PearDatabase::getInstance();
        $sql = "SELECT
                    *
                FROM
                    vtiger_insuranceplan_premium
                where
                     category_code = ?
                        AND age_limit >= ?
                        AND country_code = ?
                        AND day_limit >= ?
                        AND benefit_limit = ?
                group by plan_code
                order by age_limit asc , day_limit asc";


$sqlResult = $db->pquery($sql, array($category_code, $age_limit, $country_code, $day_limit, $benefit_limit));
        
        while ($row = $db->fetch_array($sqlResult)) {
            array_push($this->plancodearray,$row['plan_code']);
        }


        $sqlResult = $db->pquery($sql, array($category_code, $age_limit, $country_code, $day_limit, $benefit_limit));
        $result = Array();
        while ($row = $db->fetch_array($sqlResult)) {
            $row['rider'] = $this->getRiderCompaire($row['plan_code']);
            $result[] = $row;
        }

        

       
        return array_values($result);
    }
    
    function getRider($planCode){
        $db = PearDatabase::getInstance();
        $sql = "select 
        *
    from
        vtiger_insuranceplan_rider AS A
    WHERE
        A.plan_code = ? ";

        $sqlResult = $db->pquery($sql, array(strtolower($planCode)));
        $result = Array();
	while ($row = $db->fetch_array($sqlResult)) {
		if($row['amount'] == 'Covered'){
		$result['coverage'][] = $row;
		}else{
		$result['benefits'][] = $row;
		}
        }
        return $result;
    }


    function getRiderCompaire($planCode){

        
        $db = PearDatabase::getInstance();
        $sql = "select * from  vtiger_insuranceplan_rider AS A  WHERE
        A.plan_code = ?";

        $sqlResult = $db->pquery($sql, array(strtolower($planCode)));
        $result = Array();

	while ($row = $db->fetch_array($sqlResult)) {
    
		if($row['amount'] == 'Covered'||$row['amount'] == 'NA'){
        $result['coverage'][] = $row;
        
		}else{
        }
    }
  
    
    $this->plancodearray= array_unique($this->plancodearray);
    
    $Plancode1=$this->plancodearray[0];
    $Plancode2=$this->plancodearray[1];

    $sql = "SELECT * FROM vtiger_insuranceplan_rider vir1  WHERE vir1.`plan_code`='$Plancode1' AND vir1.amount<>'Covered'  AND vir1.amount<>'NA'";
        $sqlResult = $db->pquery($sql);   
	while ($row = $db->fetch_array($sqlResult)) {
        
		$result['pro'][] = $row;
    
    }
        


        $sql = "SELECT * FROM vtiger_insuranceplan_rider vir1  WHERE vir1.`plan_code`='$Plancode2' AND vir1.amount<>'Covered' AND vir1.amount<>'NA'";
        $sqlResult = $db->pquery($sql);   
	while ($row = $db->fetch_array($sqlResult)) {
       
		$result['style'][] = $row;
		
        }
        return $result  ;
    }
    function process(CustomerPortal_API_Request $request) {
        $response = new CustomerPortal_API_Response();
        $current_user = $this->getActiveUser();

        if ($current_user) {
            $record = $this->processRetrieve($request);
            $response->setResult($record);
        }
        return $response;
    }

}
