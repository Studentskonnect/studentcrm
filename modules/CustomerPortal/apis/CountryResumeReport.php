<?php

class CustomerPortal_CountryResumeReport extends CustomerPortal_API_Abstract {

    protected function processRetrieve(CustomerPortal_API_Request $request) {
	$db = PearDatabase::getInstance();
	$studentid = explode('x',$request->get('studentId')) ; 
	$studentSql = '';
	
	if(count($studentid) > 1) {
		$studentSql =  " AND vtiger_accommodation.studentid= = ". $studentid[1]."  ";
		$sql="SELECT vtiger_accommodation.accommodationid AS 'accoid',D.phone,D.firstname,D.lastname,D.email,DATE_FORMAT(vtiger_crmentity.createdtime, '%d %M %Y')AS 'createdtime',ROUND(vacf.cf_1083,2) AS 'amount',vacf.cf_1085 AS 'currency',
vacf.cf_1095 AS 'country',vacf.cf_1089 AS 'budgetfrequency',vacf.cf_1101 AS 'refrenceid'
FROM vtiger_accommodation  
INNER JOIN vtiger_crmentity ON vtiger_accommodation.accommodationid = vtiger_crmentity.crmid 
INNER JOIN vtiger_accommodationcf vacf ON vacf.accommodationid=vtiger_accommodation.accommodationid
LEFT JOIN vtiger_users ON vtiger_crmentity.smownerid = vtiger_users.id 
LEFT JOIN vtiger_groups ON vtiger_crmentity.smownerid = vtiger_groups.groupid  
LEFT JOIN vtiger_student AS D ON (vtiger_accommodation.studentid = D.studentid)
LEFT JOIN vtiger_studentcf AS E ON (vtiger_accommodation.studentid = E.studentid)
LEFT JOIN vtiger_account AS F ON (vacf.cf_1105 = F.accountid)
WHERE 
vacf.cf_1105=? $studentSql and
vtiger_crmentity.deleted=0 AND vtiger_accommodation.accommodationid > 0 and (vtiger_crmentity.createdtime BETWEEN ? AND ?)
 order by vtiger_crmentity.createdtime desc";
	}else{		
		
		$sql="SELECT vtiger_countryresume.name, vtiger_crmentity.smownerid, vtiger_crmentity.createdtime, 
			vtiger_countryresume.countryresumeid,vtiger_countryresumecf.`cf_1137` AS 'ImageUrl',
			vtiger_countryresumecf.cf_1139 AS 'Description' 
			FROM vtiger_countryresume  
			INNER JOIN vtiger_crmentity ON vtiger_countryresume.countryresumeid = vtiger_crmentity.crmid 
			INNER JOIN vtiger_countryresumecf ON vtiger_countryresumecf.countryresumeid = vtiger_countryresume.countryresumeid 
			LEFT JOIN vtiger_users ON vtiger_crmentity.smownerid = vtiger_users.id 
			LEFT JOIN vtiger_groups ON vtiger_crmentity.smownerid = vtiger_groups.groupid";
	
	}
       

//$sqlResult = $db->pquery($sql, array($this->getActiveCustomer()->id,$request->get('fromDate').' 00:00:00',$request->get('toDate').' 23.59:59'));
$sqlResult = $db->pquery($sql);
        $result = Array();

        while ($row = $db->fetch_array($sqlResult)) {
            $result[] = $row;
        }
        return $result;
    }

    function process(CustomerPortal_API_Request $request) {
        $response = new CustomerPortal_API_Response();
        $current_user = $this->getActiveUser();

        if ($current_user) {
            $record = $this->processRetrieve($request);
            $response->setResult($record);
        }
        return $response;
    }

}
