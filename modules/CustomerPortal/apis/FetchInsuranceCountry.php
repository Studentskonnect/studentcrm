<?php

/* +**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.1
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is: vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * ********************************************************************************** */

class CustomerPortal_FetchInsuranceCountry extends CustomerPortal_API_Abstract {

    protected function processRetrieve(CustomerPortal_API_Request $request) {
        $db = PearDatabase::getInstance();
        $sql = "select * from vtiger_insuranceplan_country";


        $sqlResult = $db->pquery($sql, array());
        $result = Array();
        $plans = Array();
        while ($row = $db->fetch_array($sqlResult)) {
            
            $result[] = Array(
                'country_code' => $row['country_id'],
                'country' => $row['country'],
            );
        }
        return array_values($result);
    }

    function process(CustomerPortal_API_Request $request) {
        $response = new CustomerPortal_API_Response();
        $current_user = $this->getActiveUser();

        if ($current_user) {
            $record = $this->processRetrieve($request);
            $response->setResult($record);
        }
        return $response;
    }

}

