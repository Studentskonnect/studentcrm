<?php

///Users/salim/NetBeansProjects/vt7/Student.php

$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = Vtiger_Module::getInstance('Student');
if (!$module) {
    $module = new Vtiger_Module();
    $module->name = 'Student';
    $module->parent = 'Sales';
    $module->save();
    $module->initTables();
    $module->initWebservice();
}
/*
 * 
 * Blocks for Architects Module
 * 
 */
//     info block
$infoBlock = Vtiger_Block::getInstance('LBL_STUDENT_DETAILS', $module);
if (!$infoBlock) {
    $infoBlock = new Vtiger_Block();
    $infoBlock->label = 'LBL_STUDENT_DETAILS';
    $module->addBlock($infoBlock);
}

//Description Details
$descriptionBlock = Vtiger_Block::getInstance('LBL_DESCRIPTION_INFORMATION', $module);
if (!$descriptionBlock) {
    $descriptionBlock = new Vtiger_Block();
    $descriptionBlock->label = 'LBL_DESCRIPTION_INFORMATION';
    $module->addBlock($descriptionBlock);
}
/*
 * 
 * Blocks for Architects Module END   
 * 
 */
$salutationField = Vtiger_Field::getInstance('salutationtype', $module);
if (!$salutationField) {
    $salutationField = new Vtiger_Field();
    $salutationField->name = 'salutationtype';
    $salutationField->label = 'Salutation';
    $salutationField->table = $module->basetable;
    $salutationField->column = 'salutationtype';
    $salutationField->columntype = 'VARCHAR(128)';
    $salutationField->uitype = 55;
    $salutationField->displaytype = 3;
    $salutationField->typeofdata = 'V~O';
    $infoBlock->addField($salutationField);
}

$firstnameField = Vtiger_Field::getInstance('firstname', $module);
if (!$firstnameField) {
    $firstnameField = new Vtiger_Field();
    $firstnameField->name = 'firstname';
    $firstnameField->label = 'First Name';
    $firstnameField->table = $module->basetable;
    $firstnameField->column = 'firstname';
    $firstnameField->columntype = 'VARCHAR(128)';
    $firstnameField->uitype = 55;
    $firstnameField->typeofdata = 'V~O';
    $infoBlock->addField($firstnameField);
}

$lastnameField = Vtiger_Field::getInstance('lastname', $module);
if (!$lastnameField) {
    $lastnameField = new Vtiger_Field();
    $lastnameField->name = 'lastname';
    $lastnameField->label = 'Last Name';
    $lastnameField->table = $module->basetable;
    $lastnameField->column = 'lastname';
    $lastnameField->columntype = 'VARCHAR(128)';
    $lastnameField->uitype = 255;
    $lastnameField->typeofdata = 'V~M';
    $infoBlock->addField($lastnameField);
    /** Creates the field and adds to block */
    $module->setEntityIdentifier($lastnameField);
}

/*
 * Field for State names
 */
$fatherNameField = Vtiger_Field::getInstance('father_name', $module);
if (!$fatherNameField) {
    $fatherNameField = new Vtiger_Field();
    $fatherNameField->name = 'father_name';
    $fatherNameField->label = 'Father Name';
    $fatherNameField->columntype = 'VARCHAR(100)';
    $fatherNameField->uitype = 2;
    $fatherNameField->typeofdata = 'V~O';
    $infoBlock->addField($fatherNameField);
}

/*
 * Field for State names
 */
$motherNameeField = Vtiger_Field::getInstance('mother_name', $module);
if (!$motherNameeField) {
    $motherNameeField = new Vtiger_Field();
    $motherNameeField->name = 'mother_name';
    $motherNameeField->label = 'Mother Name';
    $motherNameeField->columntype = 'VARCHAR(100)';
    $motherNameeField->uitype = 2;
    $motherNameeField->typeofdata = 'V~O';
    $infoBlock->addField($motherNameeField);
}

/*
 * Field for Contact number 
 */
$phoneField = Vtiger_Field::getInstance('phone', $module);
if (!$phoneField) {
    $phoneField = new Vtiger_Field();
    $phoneField->name = 'phone';
    $phoneField->label = 'Phone';
    $phoneField->table = $module->basetable;
    $phoneField->uitype = 11;
    $phoneField->typeofdata = 'V~M';
    $infoBlock->addField($phoneField);
}
/*
 * Field for email Address
 */
$emailField = Vtiger_Field::getInstance('email', $module);
if (!$emailField) {
    $emailField = new Vtiger_Field();
    $emailField->name = 'email';
    $emailField->label = 'Email';
    $emailField->table = $module->basetable;
    $emailField->uitype = 13;
    $emailField->typeofdata = 'E~M';
    $infoBlock->addField($emailField);
}

/*
 * Field for address deatails
 */
$addressField = Vtiger_Field::getInstance('address', $module);
if (!$addressField) {
    $addressField = new Vtiger_Field();
    $addressField->name = 'address';
    $addressField->label = 'Address';
    $addressField->table = $module->basetable;
    $addressField->columntype = 'VARCHAR(500)';
    $addressField->uitype = 21;
    $addressField->typeofdata = 'V~O';
    $infoBlock->addField($addressField);
}
/*
 * Field for State names
 */
$stateField = Vtiger_Field::getInstance('state', $module);
if (!$stateField) {
    $stateField = new Vtiger_Field();
    $stateField->name = 'state';
    $stateField->label = 'State';
    $stateField->columntype = 'VARCHAR(100)';
    $stateField->uitype = 2;
    $stateField->typeofdata = 'V~O';
    $infoBlock->addField($stateField);
}

/*
 * Field for Location names
 */
$locationField = Vtiger_Field::getInstance('city', $module);
if (!$locationField) {
    $locationField = new Vtiger_Field();
    $locationField->name = 'city';
    $locationField->label = 'Location Name';
    $locationField->columntype = 'VARCHAR(100)';
    $locationField->uitype = 2;
    $locationField->typeofdata = 'V~O';
    $infoBlock->addField($locationField);
}

/*
 * Field for PinCode 
 */
$pinCodeField = Vtiger_Field::getInstance('pincode', $module);
if (!$pinCodeField) {
    $pinCodeField = new Vtiger_Field();
    $pinCodeField->name = 'pincode';
    $pinCodeField->label = 'Postal code';
    $pinCodeField->table = $module->basetable;
    $pinCodeField->uitype = 11;
    $pinCodeField->typeofdata = 'V~O';
    $infoBlock->addField($pinCodeField);
}

/*
 * Party
 */
$relatedToField = Vtiger_Field::getInstance('contact_id', $module);
if (!$relatedToField) {
    $relatedToField = new Vtiger_Field();
    $relatedToField->name = 'contact_id';
    $relatedToField->label = 'Agent';
    $relatedToField->table = $module->basetable;
    $relatedToField->columntype = 'INT(11)';
    $relatedToField->uitype = 10;
    $relatedToField->typeofdata = 'V~M';
    $infoBlock->addField($relatedToField);
    $relatedToField->setRelatedModules(array('Contacts'));
}

/*
 * Field For Assigned To
 */
$assignedToField = Vtiger_Field::getInstance('assigned_user_id', $module);
if (!$assignedToField) {
    $assignedToField = new Vtiger_Field();
    $assignedToField->name = 'assigned_user_id';
    $assignedToField->label = 'Assigned To';
    $assignedToField->table = 'vtiger_crmentity';
    $assignedToField->column = 'smownerid';
    $assignedToField->uitype = 53;
    $assignedToField->typeofdata = 'V~M';
    $infoBlock->addField($assignedToField);
}

/*
 * Created time
 */

/*
 *
 * Architects details block Completed End
 * 
 */
$createdTimeField = Vtiger_Field::getInstance('createdtime', $module);
if (!$createdTimeField) {
    $createdTimeField = new Vtiger_Field();
    $createdTimeField->name = 'createdtime';
    $createdTimeField->label = 'Created Time';
    $createdTimeField->table = 'vtiger_crmentity';
    $createdTimeField->column = 'createdtime';
    $createdTimeField->uitype = 70;
    $createdTimeField->typeofdata = 'T~O';
    $createdTimeField->displaytype = 2;
    $infoBlock->addField($createdTimeField);
}
/*
 * Modified Time
 */
$modifiedTimeField = Vtiger_Field::getInstance('modifiedtime', $module);
if (!$modifiedTimeField) {
    $modifiedTimeField = new Vtiger_Field();
    $modifiedTimeField->name = 'modifiedtime';
    $modifiedTimeField->label = 'Modified Time';
    $modifiedTimeField->table = 'vtiger_crmentity';
    $modifiedTimeField->column = 'modifiedtime';
    $modifiedTimeField->uitype = 70;
    $modifiedTimeField->typeofdata = 'T~O';
    $modifiedTimeField->displaytype = 2;
    $infoBlock->addField($modifiedTimeField);
}

$descriptionField = Vtiger_Field::getInstance('description', $module);
if (!$descriptionField) {
    $descriptionField = new Vtiger_Field();
    $descriptionField->name = 'description';
    $descriptionField->label = 'Description';
    $descriptionField->table = 'vtiger_crmentity';
    $descriptionField->column = 'description';
    $descriptionField->uitype = 19;
    $descriptionField->typeofdata = 'V~O';
    $descriptionBlock->addField($descriptionField);
    /** Creates the field and adds to block */
}
$allFilter = Vtiger_Filter::getInstance('All', $module);
if (!$allFilter) {
    $allFilter = new Vtiger_Filter();
    $allFilter->name = 'All';
    $allFilter->isdefault = true;
    $module->addFilter($allFilter);
// Add fields to the filter created
    $allFilter->addField($salutationField)
            ->addField($firstnameField, 1)
            ->addField($lastnameField, 2)
            ->addField($fatherNameField, 3)
            ->addField($fatherNameField, 4)
            ->addField($phoneField, 5)
            ->addField($emailField, 6)
            ->addField($addressField, 7)
            ->addField($relatedToField, 8)
            ->addField($locationField, 9)
            ->addField($pinCodeField, 10);
}

global $adb;

$adb->pquery('INSERT INTO vtiger_app2tab(tabid,appname,sequence,visible) values(?,?,?,?)', array(getTabid('Student'), 'SALES', 30, 1));
$adb->pquery('CREATE TABLE `vtiger_student_user_field` (
  `recordid` int(25) NOT NULL,
  `userid` int(25) NOT NULL,
  `starred` varchar(100) DEFAULT NULL,
  KEY `fk_coachid_vtiger_student_user_field` (`recordid`),
  CONSTRAINT `fk_studentid_vtiger_student_user_field` FOREIGN KEY (`recordid`) REFERENCES `vtiger_student` (`studentid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8',Array());


/** Set sharing access of this module */
$module->setDefaultSharing('Private');
/** Enable and Disable available tools */
$module->enableTools(Array('Import'));
$module->disableTools('Merge');
