<?php

//Users/salim/NetBeansProjects/vt71/Student_relation.php

$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');
require_once('modules/ModTracker/ModTracker.php');
require_once 'modules/ModComments/ModComments.php';

$module = Vtiger_Module::getInstance('Student');
$relatedToField = Vtiger_Field::getInstance('contact_id', $module);
$commentsmodule = Vtiger_Module::getInstance('ModComments');
$fieldinstance = Vtiger_Field::getInstance('related_to', $commentsmodule);
$fieldinstance->setRelatedModules(array('Student'));
$detailviewblock = ModComments::addWidgetTo('Student');


ModTracker::enableTrackingForModule($module->id); 
//  Contacts
$contactsModule = Vtiger_Module::getInstance('Contacts');
$contactsModule->setRelatedList(
        $module, 'Students', Array('ADD'), 'get_dependents_list', $relatedToField->id
);

$documentsModule = Vtiger_Module::getInstance('Documents');
$module->setRelatedList(
        $documentsModule, 'Documents', Array('ADD', 'SELECT'), 'get_attachments'
);
