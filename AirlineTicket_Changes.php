<?php


$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = Vtiger_Module::getInstance('AirlineTicket');
$infoBlock = Vtiger_Block::getInstance('LBL_TICKET_DETAILS', $module);
/*
 * Party
 */
$relatedToField = Vtiger_Field::getInstance('account_id', $module);
if (!$relatedToField) {
    $relatedToField = new Vtiger_Field();
    $relatedToField->name = 'account_id';
    $relatedToField->label = 'Accounts';
    $relatedToField->table = $module->basetable;
    $relatedToField->columntype = 'INT(11)';
    $relatedToField->uitype = 10;
    $relatedToField->displaytype = 3;
    $relatedToField->typeofdata = 'V~O';
    $infoBlock->addField($relatedToField);
    $relatedToField->setRelatedModules(array('Accounts'));
}

$contactsModule = Vtiger_Module::getInstance('Accounts');
$contactsModule->setRelatedList(
        $module, 'Flight Tickets', Array(), 'get_dependents_list', $relatedToField->id
);
