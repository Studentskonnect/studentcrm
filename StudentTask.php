<?php

//Users/salim/NetBeansProjects/vt71/StudentTask.php

$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = Vtiger_Module::getInstance('StudentTask');
if (!$module) {
    $module = new Vtiger_Module();
    $module->name = 'StudentTask';
    $module->parent = 'Sales';
    $module->save();
    $module->initTables();
    $module->initWebservice();
}

$infoBlock = Vtiger_Block::getInstance('Task Details', $module);
if (!$infoBlock) {
    $infoBlock = new Vtiger_Block();
    $infoBlock->label = 'Task Details';
    $module->addBlock($infoBlock);
}

//Description Details
$descriptionBlock = Vtiger_Block::getInstance('LBL_DESCRIPTION_INFORMATION', $module);
if (!$descriptionBlock) {
    $descriptionBlock = new Vtiger_Block();
    $descriptionBlock->label = 'LBL_DESCRIPTION_INFORMATION';
    $module->addBlock($descriptionBlock);
}

$subjectField = Vtiger_Field::getInstance('subject', $module);
if (!$subjectField) {
    $subjectField = new Vtiger_Field();
    $subjectField->name = 'subject';
    $subjectField->label = 'Subject';
    $subjectField->table = $module->basetable;
    $subjectField->column = 'subject';
    $subjectField->columntype = 'VARCHAR(128)';
    $subjectField->uitype = 255;
    $subjectField->typeofdata = 'V~M';
    $infoBlock->addField($subjectField);
    /** Creates the field and adds to block */
    $module->setEntityIdentifier($subjectField);
}

$startDateTimeField = Vtiger_Field::getInstance('starttime', $module);
if (!$startDateTimeField) {
    $startDateTimeField = new Vtiger_Field();
    $startDateTimeField->name = 'starttime';
    $startDateTimeField->label = 'Start Date and Time';
    $startDateTimeField->table = $module->basetable;
    $startDateTimeField->column = 'starttime';
    $startDateTimeField->columntype = 'DATETIME';
    $startDateTimeField->uitype = 70;
    $startDateTimeField->typeofdata = 'DT~M';
    $startDateTimeField->displaytype = 1;
    $infoBlock->addField($startDateTimeField);
}

$endDateTimeField = Vtiger_Field::getInstance('endtime', $module);
if (!$endDateTimeField) {
    $endDateTimeField = new Vtiger_Field();
    $endDateTimeField->name = 'endtime';
    $endDateTimeField->label = 'Due Date and Time';
    $endDateTimeField->table = $module->basetable;
    $endDateTimeField->column = 'endtime';
    $endDateTimeField->columntype = 'DATETIME';
    $endDateTimeField->uitype = 70;
    $endDateTimeField->typeofdata = 'DT~M';
    $endDateTimeField->displaytype = 1;
    $infoBlock->addField($endDateTimeField);
}

$statusField = Vtiger_Field::getInstance('task_status', $module);
if (!$statusField) {
    $statusField = new Vtiger_Field();
    $statusField->name = 'task_status';
    $statusField->label = 'Status';
    $statusField->table = $module->basetable;
    $statusField->column = 'task_status';
    $statusField->columntype = 'VARCHAR(128)';
    $statusField->uitype = 16;
    $statusField->typeofdata = 'V~M';
    $infoBlock->addField($statusField);
    $statusField->setPicklistValues(array('Not Started', 'Planned', 'In Progress', 'Completed', 'Deferred'));
}

$relatedToField = Vtiger_Field::getInstance('account_id', $module);
if (!$relatedToField) {
    $relatedToField = new Vtiger_Field();
    $relatedToField->name = 'account_id';
    $relatedToField->label = 'Accounts';
    $relatedToField->table = $module->basetable;
    $relatedToField->columntype = 'INT(11)';
    $relatedToField->uitype = 10;
    $relatedToField->displaytype = 3;
    $relatedToField->typeofdata = 'V~O';
    $infoBlock->addField($relatedToField);
    $relatedToField->setRelatedModules(array('Accounts'));
}

/*
 * Party
 */
$agentField = Vtiger_Field::getInstance('contact_id', $module);
if (!$agentField) {
    $agentField = new Vtiger_Field();
    $agentField->name = 'contact_id';
    $agentField->label = 'Agent';
    $agentField->table = $module->basetable;
    $agentField->columntype = 'INT(11)';
    $agentField->uitype = 10;
    $agentField->typeofdata = 'V~M';
    $infoBlock->addField($agentField);
    $agentField->setRelatedModules(array('Contacts'));
}
/*
 * Party
 */
$relatedStudentField = Vtiger_Field::getInstance('studenttaskid', $module);
if (!$relatedStudentField) {
    $relatedStudentField = new Vtiger_Field();
    $relatedStudentField->name = 'studenttaskid';
    $relatedStudentField->label = 'Student';
    $relatedStudentField->table = $module->basetable;
    $relatedStudentField->columntype = 'INT(11)';
    $relatedStudentField->uitype = 10;
    $relatedStudentField->typeofdata = 'V~M';
    $infoBlock->addField($relatedStudentField);
    $relatedStudentField->setRelatedModules(array('Student'));
}

/*
 * Field For Assigned To
 */
$assignedToField = Vtiger_Field::getInstance('assigned_user_id', $module);
if (!$assignedToField) {
    $assignedToField = new Vtiger_Field();
    $assignedToField->name = 'assigned_user_id';
    $assignedToField->label = 'Assigned To';
    $assignedToField->table = 'vtiger_crmentity';
    $assignedToField->column = 'smownerid';
    $assignedToField->uitype = 53;
    $assignedToField->typeofdata = 'V~M';
    $infoBlock->addField($assignedToField);
}

$createdTimeField = Vtiger_Field::getInstance('createdtime', $module);
if (!$createdTimeField) {
    $createdTimeField = new Vtiger_Field();
    $createdTimeField->name = 'createdtime';
    $createdTimeField->label = 'Created Time';
    $createdTimeField->table = 'vtiger_crmentity';
    $createdTimeField->column = 'createdtime';
    $createdTimeField->uitype = 70;
    $createdTimeField->typeofdata = 'T~O';
    $createdTimeField->displaytype = 2;
    $infoBlock->addField($createdTimeField);
}
/*
 * Modified Time
 */
$modifiedTimeField = Vtiger_Field::getInstance('modifiedtime', $module);
if (!$modifiedTimeField) {
    $modifiedTimeField = new Vtiger_Field();
    $modifiedTimeField->name = 'modifiedtime';
    $modifiedTimeField->label = 'Modified Time';
    $modifiedTimeField->table = 'vtiger_crmentity';
    $modifiedTimeField->column = 'modifiedtime';
    $modifiedTimeField->uitype = 70;
    $modifiedTimeField->typeofdata = 'T~O';
    $modifiedTimeField->displaytype = 2;
    $infoBlock->addField($modifiedTimeField);
}


$descriptionField = Vtiger_Field::getInstance('description', $module);
if (!$descriptionField) {
    $descriptionField = new Vtiger_Field();
    $descriptionField->name = 'description';
    $descriptionField->label = 'Description';
    $descriptionField->table = 'vtiger_crmentity';
    $descriptionField->column = 'description';
    $descriptionField->uitype = 19;
    $descriptionField->typeofdata = 'V~O';
    $descriptionBlock->addField($descriptionField);
    /** Creates the field and adds to block */
}

$allFilter = Vtiger_Filter::getInstance('All', $module);
if (!$allFilter) {
    $allFilter = new Vtiger_Filter();
    $allFilter->name = 'All';
    $allFilter->isdefault = true;
    $module->addFilter($allFilter);
// Add fields to the filter created
    $allFilter->addField($subjectField)
            ->addField($startDateTimeField, 1)
            ->addField($endDateTimeField, 2)
            ->addField($agentField, 3)
            ->addField($relatedStudentField, 4)
            ->addField($createdTimeField, 5);
}


//  Contacts
$contactsModule = Vtiger_Module::getInstance('Contacts');
$contactsModule->setRelatedList(
        $module, 'Students', Array('ADD'), 'get_dependents_list', $agentField->id
);

$AccountsModule = Vtiger_Module::getInstance('Accounts');
$contactsModule->setRelatedList(
        $module, 'Students', Array('ADD'), 'get_dependents_list', $relatedToField->id
);

$studentModule = Vtiger_Module::getInstance('Student');
$studentModule->setRelatedList(
        $module, 'Airline Tickets', Array(), 'get_dependents_list', $relatedStudentField->id
);

global $adb;

$adb->pquery('INSERT INTO vtiger_app2tab(tabid,appname,sequence,visible) values(?,?,?,?)', array(getTabid('StudentTask'), 'SALES', 30, 1));
$adb->pquery('CREATE TABLE `vtiger_studenttask_user_field` (
  `recordid` int(25) NOT NULL,
  `userid` int(25) NOT NULL,
  `starred` varchar(100) DEFAULT NULL,
  KEY `fk_studenttaskid_vtiger_studenttask_user_field` (`recordid`),
  CONSTRAINT `fk_studenttaskid_vtiger_studenttask_user_field` FOREIGN KEY (`recordid`) REFERENCES `vtiger_studenttask` (`studenttaskid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8', Array());

$adb->pquery("insert into vtiger_customerportal_fields values (?,?,1)", array(getTabid('StudentTask'), '{"subject":1,"contact_id":0,"studentid":0,"assigned_user_id":0}'));
$adb->pquery('insert into vtiger_customerportal_tabs values (?,1,15,1,1)', array(getTabid('StudentTask')));
$adb->pquery("insert into vtiger_customerportal_prefs values (?,'showrelatedinfo',1)", array(getTabid('StudentTask')));

/** Set sharing access of this module */
$module->setDefaultSharing('Private');
/** Enable and Disable available tools */
$module->enableTools(Array('Import'));
$module->disableTools('Merge');
