<?php

$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');


$moduleNames = array('AirlineTicket', 'InsuranceCertificate', 'SimConnection');

foreach ($moduleNames as $moduleName) {
    $module = $infoBlock = $firstnameField = $lastnameField = $phoneField =  $emailField = $middlenameField =  false;
    $module = Vtiger_Module::getInstance($moduleName);
    //     info block
    $infoBlock = Vtiger_Block::getInstance('Booking Info', $module);
    
    
    if (!$infoBlock) {
        $infoBlock = new Vtiger_Block();
        $infoBlock->label = 'Booking Info';
        $module->addBlock($infoBlock);
    }

    $firstnameField = Vtiger_Field::getInstance('firstname', $module);
    if (!$firstnameField) {
        $firstnameField = new Vtiger_Field();
        $firstnameField->name = 'firstname';
        $firstnameField->label = 'First Name';
        $firstnameField->table = $module->basetable;
        $firstnameField->column = 'firstname';
        $firstnameField->columntype = 'VARCHAR(128)';
        $firstnameField->uitype = 1;
        $firstnameField->typeofdata = 'V~O';
        $firstnameField->displaytype = 2;
        $infoBlock->addField($firstnameField);
    }

    $middlenameField = Vtiger_Field::getInstance('middlename', $module);
    if (!$middlenameField) {
        $middlenameField = new Vtiger_Field();
        $middlenameField->name = 'middlename';
        $middlenameField->label = 'Middle Name';
        $middlenameField->table = $module->basetable;
        $middlenameField->column = 'middlename';
        $middlenameField->columntype = 'VARCHAR(128)';
        $middlenameField->uitype = 1;
        $middlenameField->typeofdata = 'V~O';
        $middlenameField->displaytype = 2;
        $infoBlock->addField($middlenameField);
    }


    $lastnameField = Vtiger_Field::getInstance('lastname', $module);
    if (!$lastnameField) {
        $lastnameField = new Vtiger_Field();
        $lastnameField->name = 'lastname';
        $lastnameField->label = 'Last Name';
        $lastnameField->table = $module->basetable;
        $lastnameField->column = 'lastname';
        $lastnameField->columntype = 'VARCHAR(128)';
        $lastnameField->uitype = 1;
        $lastnameField->typeofdata = 'V~O';
        $lastnameField->displaytype = 2;
        $infoBlock->addField($lastnameField);
    }
    /*
 * Field for Contact number 
 */
    $phoneField = Vtiger_Field::getInstance('phone', $module);
    if (!$phoneField) {
        $phoneField = new Vtiger_Field();
        $phoneField->name = 'phone';
        $phoneField->label = 'Phone';
        $phoneField->table = $module->basetable;
        $phoneField->uitype = 11;
        $phoneField->typeofdata = 'V~O';
        $phoneField->displaytype = 2;
        $infoBlock->addField($phoneField);
    }
    /*
 * Field for email Address
 */
    $emailField = Vtiger_Field::getInstance('email', $module);
    if (!$emailField) {
        $emailField = new Vtiger_Field();
        $emailField->name = 'email';
        $emailField->label = 'Email';
        $emailField->table = $module->basetable;
        $emailField->uitype = 13;
        $emailField->typeofdata = 'E~O';
        $emailField->displaytype = 2;
        $infoBlock->addField($emailField);
    }
}

