<?php

$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = Vtiger_Module::getInstance('SimConnection');
//     info block
$infoBlock = Vtiger_Block::getInstance('LBL_CONNECTION_DETAILS', $module);

$passportCopyField = Vtiger_Field::getInstance('passport_copy_url', $module);
if (!$passportCopyField) {
    $passportCopyField = new Vtiger_Field();
    $passportCopyField->name = 'passport_copy_url';
    $passportCopyField->label = 'Passport Copy';
    $passportCopyField->table = $module->basetable;
    $passportCopyField->column = 'passport_copy_url';
    $passportCopyField->columntype = 'VARCHAR(258)';
    $passportCopyField->uitype = 17;
    $passportCopyField->typeofdata = 'V~O';
    $infoBlock->addField($passportCopyField);
}
$ticketCopyField = Vtiger_Field::getInstance('ticket_copy_url', $module);
if (!$ticketCopyField) {
    $ticketCopyField = new Vtiger_Field();
    $ticketCopyField->name = 'ticket_copy_url';
    $ticketCopyField->label = 'Ticket Copy';
    $ticketCopyField->table = $module->basetable;
    $ticketCopyField->column = 'ticket_copy_url';
    $ticketCopyField->columntype = 'VARCHAR(258)';
    $ticketCopyField->uitype = 17;
    $ticketCopyField->typeofdata = 'V~O';
    $infoBlock->addField($ticketCopyField);
}
$visaCopyField = Vtiger_Field::getInstance('visa_copy_url', $module);
if (!$visaCopyField) {
    $visaCopyField = new Vtiger_Field();
    $visaCopyField->name = 'visa_copy_url';
    $visaCopyField->label = 'Visa Copy';
    $visaCopyField->table = $module->basetable;
    $visaCopyField->column = 'visa_copy_url';
    $visaCopyField->columntype = 'VARCHAR(258)';
    $visaCopyField->uitype = 17;
    $visaCopyField->typeofdata = 'V~M';
    $infoBlock->addField($visaCopyField);
}

