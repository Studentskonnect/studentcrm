<?php

/* +**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.1
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is: vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * ********************************************************************************** */

class CustomerPortal_StudentSimConnection extends CustomerPortal_API_Abstract {

    protected function processRetrieve(CustomerPortal_API_Request $request) {
        global $adb;
        $parentId = $request->get('studentid');

        if (!empty($parentId)) {
            if (!$this->isRecordAccessible($parentId)) {
                throw new Exception("Parent record not Accessible", 1412);
                exit;
            }

	    $db = PearDatabase::getInstance();
	    $sql = 'select simconnectionid from vtiger_simconnection JOIN vtiger_crmentity ON (simconnectionid = crmid) where deleted = 0 AND studentid = ? order by simconnectionid desc limit 1';
            $id = explode("x", $parentId);
            $studentid = $id[1];
            $sqlResult = $db->pquery($sql, array($studentid));
	    $recordId =vtws_getWebserviceEntityId('SimConnection',$db->query_result($sqlResult, 0, 'simconnectionid'));

            $result = vtws_retrieve($recordId, $this->getActiveUser());
            return $result;
        } else {

            throw new Exception("Record not Accessible", 1412);
            exit;
        }
    }

    function process(CustomerPortal_API_Request $request) {
        $response = new CustomerPortal_API_Response();
        $current_user = $this->getActiveUser();

        if ($current_user) {
            $record = $this->processRetrieve($request);

            $record = CustomerPortal_Utils::resolveRecordValues($record);
            $response->setResult(array('record' => $record));
        }
        return $response;
    }

}
