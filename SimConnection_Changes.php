<?php

$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = Vtiger_Module::getInstance('SimConnection');
//     info block
$infoBlock = Vtiger_Block::getInstance('LBL_CONNECTION_DETAILS', $module);

$passportBackCopyField = Vtiger_Field::getInstance('passport_back_copy_url', $module);
if (!$passportBackCopyField) {
    $passportBackCopyField = new Vtiger_Field();
    $passportBackCopyField->name = 'passport_back_copy_url';
    $passportBackCopyField->label = 'Passport Back Copy';
    $passportBackCopyField->table = $module->basetable;
    $passportBackCopyField->column = 'passport_back_copy_url';
    $passportBackCopyField->columntype = 'VARCHAR(258)';
    $passportBackCopyField->uitype = 17;
    $passportBackCopyField->typeofdata = 'V~O';
    $infoBlock->addField($passportBackCopyField);
}
