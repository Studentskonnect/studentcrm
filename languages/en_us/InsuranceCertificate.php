<?php
/*+**********************************************************************************
 * Created by : Salim
 * Email : salimcmd@gmail.com, salim@oztro.com
 ************************************************************************************/
$languageStrings = array(
// Basic Strings
    'InsuranceCertificate' => 'Insurance Certificates',
    'SINGLE_InsuranceCertificate' => 'Insurance Certificate',
    'LBL_ADD_RECORD' => 'Add Insurance Certificate',
    'LBL_RECORDS_LIST' => 'Insurance Certificate List',
    // Blocks
    'LBL_POLICY_DETAILS' => 'Insurance Policy Details',
    //Field Labels
    "Phone" => "Phone",
);

$jsLanguageStrings = array();
