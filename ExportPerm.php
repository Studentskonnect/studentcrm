<?php
$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$modules = array('AirlineTicket', 'SimConnection', 'InsuranceCertificate');
foreach ($modules as $moduleName) {
    $module = Vtiger_Module::getInstance($moduleName);
    $module->enableTools(Array('Import','Export'));
    $module->disableTools('Merge');
}
