<?php

//Users/salim/NetBeansProjects/vt71/AirlineTicket.php

$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = Vtiger_Module::getInstance('AirlineTicket');

$infoBlock = Vtiger_Block::getInstance('LBL_TICKET_DETAILS', $module);

$traceField = Vtiger_Field::getInstance('trace_id', $module);
if (!$traceField) {
    $traceField = new Vtiger_Field();
    $traceField->name = 'trace_id';
    $traceField->label = 'Trace ID';
    $traceField->table = $module->basetable;
    $traceField->column = 'trace_id';
    $traceField->columntype = 'VARCHAR(128)';
    $traceField->uitype = 255;
    $traceField->typeofdata = 'V~M';
    $infoBlock->addField($traceField);
}
